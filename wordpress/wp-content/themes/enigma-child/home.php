<?php get_header(); ?>
<div class="enigma_header_breadcrum_title">	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php if(is_home()){echo "";}else{the_title();} ?></h1>
				<?php if( is_home() && get_option('page_for_posts') ) : ?>
				<header class="entry-header">
					<h1 class="entry-title"><?php echo apply_filters('the_title',get_page( get_option('page_for_posts') )->post_title); ?></h1>
				</header>
				<?php endif; ?>
			</div>
		</div>
	</div>	
</div>
<div class="container">	
	<div class="row enigma_blog_wrapper">
	<div class="col-md-8">
	<?php if ( have_posts()): 
	while ( have_posts() ): the_post(); ?>	
	<?php get_template_part('post','content'); ?>	
	<?php endwhile; 
	endif; ?>
	<?php weblizar_navigation(); ?>	
	</div>
	<?php get_sidebar(); ?>
	</div>	
</div>
<?php get_footer(); ?>