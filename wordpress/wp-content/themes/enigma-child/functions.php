<?php
/*
  Enigma Child functions and definitions
 
  @package enigma_child
 * @since enigma_child 1.0
 */
 
 /*
	* child widget area
	*/
	add_action( 'widgets_init', 'child_widgets_init');
	function child_widgets_init() {
	/*sidebar*/
	
	register_sidebar( array(
			'name' => __( 'Header Widget Area', 'child' ),
			'id' => 'header-widget-area',
			'description' => __( 'header widget area', 'child' ),
			'before_widget' => '<div class="enigma_header_widget_column">',
			'after_widget' => '</div>',
			'before_title' => '<div class="enigma_header_widget_title">',
			'after_title' => '<div class="enigma-header-separator"></div></div>',
		) );             
	}

