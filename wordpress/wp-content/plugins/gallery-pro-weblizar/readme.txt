=== Gallery Pro Weblizar ===
Contributors: weblizar
Donate link: http://www.weblizar.com/
Tags: add gallery, add image, add images, add photo, add picture, add pictures, album, art gallery, banner rotator, best gallery, best gallery plugin, content gallery, content slider, easy gallery, filterable gallery, photo, photoalbum, photogalerie, free gallery, free images, free photo gallery, free slider, fullscreen gallery, fullscreen slider, gallery, gallery, gallery, gallery, Galleria, galleries, gallery, gallery decription, gallery image, gallery lightbox, Gallery Plugin, gallery shortcode, gallery slider, gallery wordpress, gelary, grid, grid gallery, image, image album, image gallery, image gallery plugin, image slider, images, images gallery, iphone gallery, jquery gallery, justified, justified gallery, lightbox gallery, Lightbox slider, media gallery, multiple pictures, photo album, photo albums, photo gallery, photoalbum, photogallery, photography, photoset, Picture Gallery, plugin gallery, Post, posts, responsive, responsive galleries, responsive gallery, responsive wordpress photo gallery, revolution, shortcode gallery, Simple gallery, slide, slideshow, slideshow gallery, thumbnail view, thumbs, video gallery, vimeo gallery, website gallery, widget gallery, wordpress gallery, wordpress gallery plugin, wordpress photo gallery plugin, wp gallery, wp gallery plugins, youtube gallery, , css3 animation, masonry gallery, link gallery, external link, social gallery, seo gallery, 
Requires at least: 3.5
Tested up to: 4.3
Stable tag: 5.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin allows you to implement a photo gallery, video gallery and link gallery page into your website. 

== Description ==

Design various types of image, video, link gallery using this plugin. 
Using **[WG id=123]** shortcode, you can publish gallery in any Page or Post in your WordPress sites.

https://www.youtube.com/watch?v=Mpg5V4TTEzc

**Plugin Demo:** [Gallery Free Demo](http://demo.weblizar.com/gallery-pro/)

> **Plugin Features**
>
> * Multiple Image Uploader
> * Multiple Shortcode
> * Four Animation Effect
> * Responsive Gallery Plugin
> * Two & Four Column Layout
> * Background Color Option
> * Various Font Style
> * Lightbox Preview
> * Custom CSS Option
> * Video Gallery
> * External Link
> * Add Description
> * Hide/Show Gallery Title and Label
> * Unique Settings for Each Gallery
> * Shortcode Button On Post or Page
> * Drag and Drop Image Position


### Gallery Pro Features:
A Perfect Responsive Gallery Pro Plugin for WordPress where you are able to Display your WordPress content like Photo, Video, Link, Image etc in Gallery format Display With Lightbox.


> * Responsive Design
> * Multiple Image Uploader
> * Multiple Shortcode    
> * Youtube/Vimeo Video
> * Two, Three & Four Column Layou
> * Fancybox,Nivo box, Prettyphoto or Swipebox Lightbox Preview
> * Custom CSS Option
> * Video Gallery
> * External Link
> * Add Description
> * Hide/Show Gallery Title and Label
> * Unique Settings for Each Gallery
> * Shortcode Button On Post or Page
> * Drag and Drop Image Position
>* And many more..
>

Gallery Pro is very flexible and and compatible on all devices like ipad,iphone smart phone etc.

> #### **Live Demos**


> * [DEMO : Grid Gallery](http://demo.weblizar.com/gallery-pro/)
> * [DEMO : Vimeo Video Gallery](http://demo.weblizar.com/gallery-pro/vimeo-video-gallery/)
> * [DEMO : Youtube Video Gallery](http://demo.weblizar.com/gallery-pro/youtube-video-gallery/)
> * [DEMO : External link Portfolio](http://demo.weblizar.com/gallery-pro/link-gallery/)
> * [DEMO : Swipe Box](http://demo.weblizar.com/photo-video-link-gallery-pro/swipe-box/)


### Plugin Admin Features

Plugin has extreme Admin dashboard. Plugin settings is very easy and user friendly.With Multiple Image Uploader you are easily add multiple images in seconds

> ### Try Gallery Pro Demo: [Live Demo](http://demo.weblizar.com/gallery-pro/)
>
> ### Try Gallery Pro Admin: [Admin Demo](http://demo.weblizar.com/gallery-pro-admin-demo/)
>
>### Upgrade To: [Photo Video Link Gallery Pro](http://weblizar.com/plugins/gallery-pro/)




If you have any question contact us at here: [Plugin Support Forum ](http://wordpress.org/support/plugin/photo-gallery-plugin) 



== Installation ==

1. Upload the entire **photo-gallery-plugin** folder to the **/wp-content/plugins/** directory.
2. Activate the plugin through the **Plugins** menu in WordPress admin.
3. Create new gallery, Use **[WG id=123]** shortcode to publish your gallery into any page or post.

== Screenshots ==

1. Gallery Preview on Site
2. Gallery Full Preview 
3. Gallery Image Preview Light Box
4. Add Images into Gallery
6. Gallery Settings
7. Insert Images into Gallery




== Changelog ==

For more information, see Weblizar(http://wwww.weblizar.com/).

= Version 1.1 01-09-2015 =

* Add New Transition Effect.
* Add Multiple Colour Picker for Hover Effect.
* Add Masonry Effect for Gallery.  
 

= Version 1.0 08-05-2015 =

* This is first and basic version of gallery plugin.


= Docs & Support =

If any support required then post your query in WordPress [Plugin Support Forum ](http://wordpress.org/support/plugin/photo-gallery-plugin)

= We Need Your Support =

It is really hard to continue development and support for this free plugin without contributions from users like you. If you are enjoying using our Responsive Gallery plugin and find it useful, then please consider to write your positive [__Feedback__](http://wordpress.org/support/view/plugin-reviews/photo-gallery-plugin). Your feedback will help us to encourage and support the plugin's continued development and better user support.

= Translators =

Please contribute to translate our plugin.  Contact at `lizarweb (at) gmail (dot) com`.

== Frequently Asked Questions ==

Please use WordPress [support forum](http://wordpress.org/support/plugin/photo-gallery-plugin) to ask any query regarding any issue.