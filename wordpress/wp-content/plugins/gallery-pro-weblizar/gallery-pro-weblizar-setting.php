<?php 
$postid=$post->ID;
$lksg_Gallery_Settings = "lksg_Gallery_Settings_".$postid;
$lksg_Gallery_Settings = unserialize(get_post_meta( $postid, $lksg_Gallery_Settings, true));
	//print_r($lksg_Gallery_Settings);
if($lksg_Gallery_Settings['wgp_Color'] && $lksg_Gallery_Settings['lk_gallery_title'])
{
	$wgp_Color	            =$lksg_Gallery_Settings['wgp_Color'];
	$lk_show_Gallery_title	=$lksg_Gallery_Settings['lk_gallery_title'];
	$lk_show_image_label	=$lksg_Gallery_Settings['lk_show_image_label'];
	$lk_show_gallery_layout	=$lksg_Gallery_Settings['lk_Gallery_Layout'];
	$lk_label_Color			=$lksg_Gallery_Settings['lk_label_Color'];
	$lk_desc_font_Color		=$lksg_Gallery_Settings['lk_desc_font_Color'];
	$lk_btn_Color			=$lksg_Gallery_Settings['lk_btn_Color'];
	$lk_btn_font_Color		=$lksg_Gallery_Settings['lk_btn_font_Color'];
	$lk_font_style 			=$lksg_Gallery_Settings['lk_font_style'];
	$lk_Custom_CSS			=$lksg_Gallery_Settings['lk_Custom_CSS'];
	$lk_show_img_desc       =$lksg_Gallery_Settings['lk_show_img_desc'];
	$lk_Light_Box      	    =$lksg_Gallery_Settings['lk_Light_Box'];
	$lk_open_link 		    =$lksg_Gallery_Settings['lk_open_link'];
	$lk_button_title		=$lksg_Gallery_Settings['lk_button_title'];

}else{
	$wgp_Color					="#e3e3e3";
	$lk_show_Gallery_title		="Yes";
	$lk_show_image_label		="Yes";
	$lk_show_gallery_layout		="3";
	$lk_label_Color			    ="#666";
	$lk_desc_font_Color			="#777777";
	$lk_btn_Color			    ="#428bca";
	$lk_btn_font_Color			="#FFF";
	$lk_font_style				="Courgette";
	$lk_Custom_CSS				="";
	$lk_show_img_desc			="Yes";
	$lk_Light_Box				="lightbox3";
	$lk_open_link            	="_blank";
	$lk_button_title			="Zoom";
}	

?>

<input type="hidden" id="lksg_action" name="lksg_action" value="wl-wrgf-save-settings">
<table class=" form-table" >
	<tr>
		<th><label>Show Gallery Title</label></th>
		<td>
			<input type="radio" name="lk-show-gallery-title" id="lk-show-gallery-title" value="Yes" <?php if($lk_show_Gallery_title=="Yes") {echo "checked";} ?>><i class="fa fa-check fa-2x"></i> 


			<input type="radio" name="lk-show-gallery-title" id="lk-show-gallery-title" value="no" <?php if($lk_show_Gallery_title=="no") {echo "checked";} ?>><i class="fa fa-times fa-2x" ></i>


			<p>Select Yes/No option to hide or show gallery title</p>
		</td>
	</tr>
	<tr>
		<th><label>Show Image Label</label></th>
		<td>
			<input type="radio" name="lk-show-image-label" id="lk-show-image-label" value="Yes" <?php if($lk_show_image_label=="Yes") {echo "checked";} ?>><i class="fa fa-check fa-2x"></i> 

			<input type="radio" name="lk-show-image-label" id="lk-show-image-label" value="no" <?php if($lk_show_image_label=="no") {echo "checked";} ?>>
			<i class="fa fa-times fa-2x"></i>
			<p>Select Yes/No option to hide or show gallery label</p>
		</td>
	</tr>
	<tr>
		<th><label>Show Image Description</label></th>
		<td>
			<input type="radio" name="lk_show_img_desc" id="lk_show_img_desc" value="Yes" <?php if($lk_show_img_desc=="Yes") {echo "checked";} ?>><i class="fa fa-check fa-2x"></i> 

			<input type="radio" name="lk_show_img_desc" id="lk_show_img_descc" value="no" <?php if($lk_show_img_desc=="no") {echo "checked";} ?>>
			<i class="fa fa-times fa-2x"></i>
			<p>Select Yes/No option to hide or show gallery description</p>
		</td>
	</tr>
	<tr>
		<th scope="row"><label>Open Link</label></th>
		<td>
			
			<input type="radio" name="lk_open_link" id="lk_open_link" value="_self" <?php if($lk_open_link == '_self' ) { echo "checked"; } ?>> In Same Tab 
			<input type="radio" name="lk_open_link" id="lk_open_link" value="_blank" <?php if($lk_open_link == '_blank' ) { echo "checked"; } ?>> In New Tab

			<p class="description">
				Select option to open link in save tab or in new tab.
			</p>
		</td>
	</tr>		

	<tr>
		<th><label>Gallery Layout</label></th>
		<td>
			<select name="lk-gallery-layout" id="lk-gallery-layout">
				<optgroup label="Select Gallery Layout">
					
					<option value="2" <?php if($lk_show_gallery_layout=="2") {echo "selected";} ?> >Two Column</option>
					<option value="3" <?php if($lk_show_gallery_layout=="3") {echo "selected";} ?>>Three Column</option>
					<option value="4" <?php if($lk_show_gallery_layout=="4") {echo "selected";} ?>>Four Column</option>
					<option value="5" <?php if($lk_show_gallery_layout=="5") {echo "selected";} ?>>Five Column</option>
					<option value="6" <?php if($lk_show_gallery_layout=="6") {echo "selected";} ?>>Six Column</option>
					<option value="7" <?php if($lk_show_gallery_layout=="7") {echo "selected";} ?>>Seven Column</option>
					<option value="8" <?php if($lk_show_gallery_layout=="8") {echo "selected";} ?>>Eight Column</option>
					<option value="9" <?php if($lk_show_gallery_layout=="9") {echo "selected";} ?>>Nine Column</option>
					<option value="10" <?php if($lk_show_gallery_layout=="10") {echo "selected";} ?>>Ten Column</option>
				</optgroup>
			</select>
			<p>Choose a column layout for image gallery.</p>
		</td>
	</tr>
	<tr>
		<th><label>Image Background Color</label></th>
		<td>

			<input id="wgp_Color" name="wgp_Color" type="text" value="<?php echo $wgp_Color; ?>" class="my-color-field" data-default-color="#e3e3e3" />
			<p class="description">
				Select any color to apply on image background.

			</p>
		</td>
	</tr>	

	<tr>
		<th><label>Image Label Color</label></th>
		<td>	
			<input id="lk_label_Color" name="lk_label_Color" type="text" value="<?php echo $lk_label_Color; ?>" class="my-color-field" data-default-color="#666" />
			<p class="description">
				Select any color to apply on image Label.

			</p>
		</td>
	</tr>

	<tr>
		<th><label>Description Font Color</label></th>
		<td>	
			<input id="lk_desc_font_Color" name="lk_desc_font_Color" type="text" value="<?php echo $lk_desc_font_Color; ?>" class="my-color-field" data-default-color="#777777" />

			<p class="description">
				Select any color to apply on image description font color.

			</p>
		</td>
	</tr>
	<tr>
		<th><label>Button Background Color</label></th>
		<td>	
			<input id="lk_btn_Color" name="lk_btn_Color" type="text" value="<?php echo $lk_btn_Color; ?>" class="my-color-field" data-default-color="#428bca" />

			<p class="description">
				Select any color to apply on button background color .

			</p>
		</td>
	</tr>	
	<tr>
		<th><label>Button Font Color</label></th>
		<td>	
			<input id="lk_btn_font_Color" name="lk_btn_font_Color" type="text" value="<?php echo $lk_btn_font_Color; ?>" class="my-color-field" data-default-color="#FFF" />

			<p class="description">
				Select any color to apply on button font color.

			</p>
		</td>
	</tr>
	
	<tr>
		<th scope="row">Font Style</label></th>
		<td>
			<select name="lk_font_style" id="lk_font_style">
				<optgroup label="Default Fonts">
					<option value="Arial" <?php selected($lk_font_style, 'Arial' ); ?>>Arial</option>
					<option value="_arial_black" <?php selected($lk_font_style, '_arial_black' ); ?>>Arial Black</option>
					<option value="Courier New" <?php selected($lk_font_style, 'Courier New' ); ?>>Courier New</option>
					<option value="georgia" <?php selected($lk_font_style, 'Georgia' ); ?>>Georgia</option>
					<option value="grande" <?php selected($lk_font_style, 'Grande' ); ?>>Grande</option>
					<option value="_helvetica_neue" <?php selected($lk_font_style, '_helvetica_neue' ); ?>>Helvetica Neue</option>
					<option value="_impact" <?php selected($lk_font_style, '_impact' ); ?>>Impact</option>
					<option value="_lucida" <?php selected($lk_font_style, '_lucida' ); ?>>Lucida</option>
					<option value="_lucida" <?php selected($lk_font_style, '_lucida' ); ?>>Lucida Grande</option>
					<option value="_OpenSansBold" <?php selected($lk_font_style, 'OpenSansBold' ); ?>>OpenSansBold</option>
					<option value="_palatino" <?php selected($lk_font_style, '_palatino' ); ?>>Palatino</option>
					<option value="_sans" <?php selected($lk_font_style, '_sans' ); ?>>Sans</option>
					<option value="_sans" <?php selected($lk_font_style, 'Sans-Serif' ); ?>>Sans-Serif</option>
					<option value="_tahoma" <?php selected($lk_font_style, '_tahoma' ); ?>>Tahoma</option>
					<option value="_times"<?php selected($lk_font_style, '_times' ); ?>>Times New Roman</option>
					<option value="_trebuchet" <?php selected($lk_font_style, 'Trebuchet' ); ?>>Trebuchet</option>
					<option value="_verdana" <?php selected($lk_font_style, '_verdana' ); ?>>Verdana</option>
				</optgroup>
				<optgroup label="Google Fonts">
					<option value="Abel"<?php selected($lk_font_style, 'Abel' ); ?>>Abel</option>
					<option value="Abril Fatface" <?php selected($lk_font_style, 'Abril Fatface' ); ?>>Abril Fatface</option>
					<option value="Aclonica" <?php selected($lk_font_style, 'Aclonica' ); ?>>Aclonica</option>
					<option value="Acme" <?php selected($lk_font_style, 'Acme' ); ?>>Acme</option>
					<option value="Actor" <?php selected($lk_font_style, 'Actor' ); ?>>Actor</option>
					<option value="Adamina" <?php selected($lk_font_style, 'Adamina' ); ?>>Adamina</option>
					<option value="Advent Pro" <?php selected($lk_font_style, 'Advent Pro' ); ?>>Advent Pro</option>
					<option value="Aguafina Script" <?php selected($lk_font_style, 'Aguafina Script' ); ?>>Aguafina Script</option>
					<option value="Aladin" <?php selected($lk_font_style, 'Aladin' ); ?>>Aladin</option>
					<option value="Aldrich" <?php selected($lk_font_style, 'Aldrich' ); ?>>Aldrich</option>
					<option value="Alegreya" <?php selected($lk_font_style, 'Alegreya' ); ?>>Alegreya</option>
					<option value="Alegreya SC" <?php selected($lk_font_style, 'Alegreya SC' ); ?>>Alegreya SC</option>
					<option value="Alex Brush" <?php selected($lk_font_style, 'Alex Brush' ); ?>>Alex Brush</option>
					<option value="Alfa Slab One" <?php selected($lk_font_style, 'Alfa Slab One' ); ?>>Alfa Slab One</option>
					<option value="Alice" <?php selected($lk_font_style, 'Alice' ); ?>>Alice</option>
					<option value="Alike" <?php selected($lk_font_style, 'Alike' ); ?>>Alike</option>
					<option value="Alike Angular" <?php selected($lk_font_style, 'Alike Angular' ); ?>>Alike Angular</option>
					<option value="Allan" <?php selected($lk_font_style, 'Allan' ); ?>>Allan</option>
					<option value="Allerta" <?php selected($lk_font_style, 'Allerta' ); ?>>Allerta</option>
					<option value="Allerta Stencil"<?php selected($lk_font_style, 'Allerta Stencil' ); ?>>Allerta Stencil</option>
					<option value="Allura" <?php selected($lk_font_style, 'Allura' ); ?>>Allura</option>
					<option value="Almendra" <?php selected($lk_font_style, 'Almendra' ); ?>>Almendra</option>
					<option value="Almendra SC"<?php selected($lk_font_style, 'Almendra SC' ); ?>>Almendra SC</option>
					<option value="Amaranth" <?php selected($lk_font_style, 'Amaranth' ); ?>>Amaranth</option> <option value="Amatic SC"<?php selected($lk_font_style, 'Amatic SC' ); ?>>Amatic SC</option>
					<option value="Amethysta" <?php selected($lk_font_style, 'Amethysta' ); ?>>Amethysta</option><option value="Andada"<?php selected($lk_font_style, 'Andada' ); ?>>Andada</option>
					<option value="Andika" <?php selected($lk_font_style, 'Andika' ); ?>>Andika</option>
					<option value="Angkor" <?php selected($lk_font_style, 'Angkor' ); ?>>Angkor</option>
					<option value="Annie Use Your Telescope" <?php selected($lk_font_style, 'Annie Use Your Telescope' ); ?>>Annie Use Your Telescope</option>
					<option value="Anonymous Pro" <?php selected($lk_font_style, 'Anonymous Pro' ); ?>>Anonymous Pro</option>
					<option value="Antic" <?php selected($lk_font_style, 'Antic' ); ?>>Antic</option>
					<option value="Antic Didone" <?php selected($lk_font_style, 'Antic Didone' ); ?>>Antic Didone</option>
					<option value="Antic Slab" <?php selected($lk_font_style, 'Antic Slab' ); ?>>Antic Slab</option>
					<option value="Anton" <?php selected($lk_font_style, 'Anton' ); ?>>Anton</option>
					<option value="Arapey" <?php selected($lk_font_style, 'Arapey' ); ?>>Arapey</option>
					<option value="Arbutus" <?php selected($lk_font_style, 'Arbutus' ); ?>>Arbutus</option>
					<option value="Architects Daughter" <?php selected($lk_font_style, 'Architects Daughter' ); ?>>Architects Daughter</option>
					<option value="Arimo" <?php selected($lk_font_style, 'Arimo' ); ?>>Arimo</option>
					<option value="Arizonia" <?php selected($lk_font_style, 'Arizonia' ); ?>>Arizonia</option>
					<option value="Armata" <?php selected($lk_font_style, 'Armata' ); ?>>Armata</option>
					<option value="Artifika" <?php selected($lk_font_style, 'Artifika' ); ?>>Artifika</option>
					<option value="Arvo" <?php selected($lk_font_style, 'Arvo' ); ?>>Arvo</option>
					<option value="Asap" <?php selected($lk_font_style, 'Asap' ); ?>>Asap</option>
					<option value="Asset" <?php selected($lk_font_style, 'Asset' ); ?>>Asset</option>
					<option value="Astloch" <?php selected($lk_font_style, 'Astloch' ); ?>>Astloch</option>
					<option value="Asul" <?php selected($lk_font_style, 'Asul' ); ?>>Asul</option>
					<option value="Atomic Age" <?php selected($lk_font_style, 'Atomic Age' ); ?>>Atomic Age</option>
					<option value="Aubrey" <?php selected($lk_font_style, 'Aubrey' ); ?>>Aubrey</option>
					<option value="Audiowide" <?php selected($lk_font_style, 'Audiowide' ); ?>>Audiowide</option>
					<option value="Average" <?php selected($lk_font_style, 'Average' ); ?>>Average</option>
					<option value="Averia Gruesa Libre" <?php selected($lk_font_style, 'Averia Gruesa Libre' ); ?>>Averia Gruesa Libre</option>
					<option value="Averia Libre" <?php selected($lk_font_style, 'Averia Libre' ); ?>>Averia Libre</option>
					<option value="Averia Sans Libre" <?php selected($lk_font_style, 'Averia Sans Libre' ); ?>>Averia Sans Libre</option>
					<option value="Averia Serif Libre" <?php selected($lk_font_style, 'Averia Serif Libre' ); ?>>Averia Serif Libre</option>
					<option value="Bad Script" <?php selected($lk_font_style, 'Bad Script' ); ?>>Bad Script</option>
					<option value="Balthazar" <?php selected($lk_font_style, 'Balthazar' ); ?>>Balthazar</option>
					<option value="Bangers" <?php selected($lk_font_style, 'Bangers' ); ?>>Bangers</option>
					<option value="Basic" <?php selected($lk_font_style, 'Basic' ); ?>>Basic</option>
					<option value="Battambang" <?php selected($lk_font_style, 'Battambang' ); ?>>Battambang</option>
					<option value="Baumans" <?php selected($lk_font_style, 'Baumans' ); ?>>Baumans</option>
					<option value="Bayon" <?php selected($lk_font_style, 'Bayon' ); ?>>Bayon</option>
					<option value="Belgrano"<?php selected($lk_font_style, 'Belgrano' ); ?>>Belgrano</option>
					<option value="Belleza" <?php selected($lk_font_style, 'Belleza' ); ?>>Belleza</option>
					<option value="Bentham" <?php selected($lk_font_style, 'Bentham' ); ?>>Bentham</option>
					<option value="Berkshire Swash"<?php selected($lk_font_style, 'Berkshire Swash' ); ?>>Berkshire Swash</option>
					<option value="Bevan"  <?php selected($lk_font_style, 'Bevan' ); ?>>Bevan</option>
					<option value="Bigshot One"<?php selected($lk_font_style, 'Bigshot One' ); ?>>Bigshot One</option>
					<option value="Bilbo" <?php selected($lk_font_style, 'Bilbo' ); ?>>Bilbo</option>
					<option value="Bilbo Swash Caps" <?php selected($lk_font_style, 'Bilbo Swash Caps' ); ?>>Bilbo Swash Caps</option>
					<option value="Bitter" <?php selected($lk_font_style, 'Bitter' ); ?>>Bitter</option>
					<option value="Black Ops One" <?php selected($lk_font_style, 'Black Ops One' ); ?>>Black Ops One</option>
					<option value="Bokor" <?php selected($lk_font_style, 'Bokor' ); ?>>Bokor</option>
					<option value="Bonbon" <?php selected($lk_font_style, 'Bonbon' ); ?>>Bonbon</option>
					<option value="Boogaloo" <?php selected($lk_font_style, 'Boogaloo' ); ?>>Boogaloo</option>
					<option value="Bowlby One" <?php selected($lk_font_style, 'Bowlby One' ); ?>>Bowlby One</option>
					<option value="Bowlby One SC" <?php selected($lk_font_style, 'Bowlby One SC' ); ?>>Bowlby One SC</option>
					<option value="Brawler" <?php selected($lk_font_style, 'Brawler' ); ?>>Brawler</option>
					<option value="Bree Serif" <?php selected($lk_font_style, 'Bree Serif' ); ?>>Bree Serif</option>
					<option value="Bubblegum Sans"  <?php selected($lk_font_style, 'Bubblegum Sans' ); ?>>Bubblegum Sans</option>
					<option value="Buda"  <?php selected($lk_font_style, 'Buda' ); ?>>Buda</option>
					<option value="Buenard"  <?php selected($lk_font_style, 'Buenard' ); ?>>Buenard</option>
					<option value="Butcherman"  <?php selected($lk_font_style, 'Butcherman' ); ?>>Butcherman</option>
					<option value="Butterfly Kids" <?php selected($lk_font_style, 'Butterfly Kids' ); ?>>Butterfly Kids</option>
					<option value="Cabin"  <?php selected($lk_font_style, 'Cabin' ); ?>>Cabin</option>
					<option value="Cabin Condensed"  <?php selected($lk_font_style, 'Cabin Condensed' ); ?>>Cabin Condensed</option>
					<option value="Cabin Sketch"  <?php selected($lk_font_style, 'Cabin Sketch' ); ?>>Cabin Sketch</option>
					<option value="Caesar Dressing"  <?php selected($lk_font_style, 'Caesar Dressing' ); ?>>Caesar Dressing</option>
					<option value="Cagliostro"  <?php selected($lk_font_style, 'Cagliostro' ); ?>>Cagliostro</option>
					<option value="Calligraffitti"  <?php selected($lk_font_style, 'Calligraffitti' ); ?>>Calligraffitti</option>
					<option value="Cambo"  <?php selected($lk_font_style, 'Cambo' ); ?>>Cambo</option>
					<option value="Candal"  <?php selected($lk_font_style, 'Candal' ); ?>>Candal</option>
					<option value="Cantarell"  <?php selected($lk_font_style, 'Cantarell' ); ?>>Cantarell</option>
					<option value="Cantata One"  <?php selected($lk_font_style, 'Cantata One' ); ?>>Cantata One</option>
					<option value="Cardo"  <?php selected($lk_font_style, 'Cardo' ); ?>>Cardo</option>
					<option value="Carme"  <?php selected($lk_font_style, 'Carme' ); ?>>Carme</option>
					<option value="Carter One"  <?php selected($lk_font_style, 'Carter One' ); ?>>Carter One</option>
					<option value="Caudex"  <?php selected($lk_font_style, 'Caudex' ); ?>>Caudex</option>
					<option value="Cedarville Cursive"  <?php selected($lk_font_style, 'Cedarville Cursive' ); ?>>Cedarville Cursive</option>
					<option value="Ceviche One"  <?php selected($lk_font_style, 'Ceviche One' ); ?>>Ceviche One</option>
					<option value="Changa One"  <?php selected($lk_font_style, 'Changa One' ); ?>>Changa One</option>
					<option value="Chango"  <?php selected($lk_font_style, 'Chango' ); ?>>Chango</option>
					<option value="Chau Philomene One"  <?php selected($lk_font_style, 'Chau Philomene One' ); ?>>Chau Philomene One</option>
					<option value="Chelsea Market"  <?php selected($lk_font_style, 'Chelsea Market' ); ?>>Chelsea Market</option>
					<option value="Chenla"  <?php selected($lk_font_style, 'Chenla' ); ?>>Chenla</option>
					<option value="Cherry Cream Soda"  <?php selected($lk_font_style, 'Cherry Cream Soda' ); ?>>Cherry Cream Soda</option>
					<option value="Chewy"  <?php selected($lk_font_style, 'Chewy' ); ?>>Chewy</option>
					<option value="Chicle"  <?php selected($lk_font_style, 'Chicle' ); ?>>Chicle</option>
					<option value="Chivo"  <?php selected($lk_font_style, 'Chivo' ); ?>>Chivo</option>
					<option value="Coda"  <?php selected($lk_font_style, 'Coda' ); ?>>Coda</option>
					<option value="Coda Caption"  <?php selected($lk_font_style, 'Coda Caption' ); ?>>Coda Caption</option>
					<option value="Codystar"  <?php selected($lk_font_style, 'Codystar' ); ?>>Codystar</option>
					<option value="Comfortaa"  <?php selected($lk_font_style, 'Comfortaa' ); ?>>Comfortaa</option>
					<option value="Coming Soon"  <?php selected($lk_font_style, 'Coming Soon' ); ?>>Coming Soon</option>
					<option value="Concert One"  <?php selected($lk_font_style, 'Concert One' ); ?>>Concert One</option>
					<option value="Condiment"  <?php selected($lk_font_style, 'Condiment' ); ?>>Condiment</option>
					<option value="Content"  <?php selected($lk_font_style, 'Content' ); ?>>Content</option>
					<option value="Contrail One"  <?php selected($lk_font_style, 'Contrail One' ); ?>>Contrail One</option>
					<option value="Convergence"  <?php selected($lk_font_style, 'Convergence' ); ?>>Convergence</option>
					<option value="Cookie"  <?php selected($lk_font_style, 'Cookie' ); ?>>Cookie</option>
					<option value="Copse"  <?php selected($lk_font_style, 'Copse' ); ?>>Copse</option>
					<option value="Corben"  <?php selected($lk_font_style, 'Corben' ); ?>>Corben</option>
					<option value="Courgette"  <?php selected($lk_font_style, 'Courgette' ); ?>>Courgette</option>
					<option value="Cousine"  <?php selected($lk_font_style, 'Cousine' ); ?>>Cousine</option>
					<option value="Coustard"  <?php selected($lk_font_style, 'Coustard' ); ?>>Coustard</option>
					<option value="Covered By Your Grace"  <?php selected($lk_font_style, 'Covered By Your Grace' ); ?>>Covered By Your Grace</option>
					<option value="Crafty Girls"  <?php selected($lk_font_style, 'Crafty Girls' ); ?>>Crafty Girls</option>
					<option value="Creepster"  <?php selected($lk_font_style, 'Creepster' ); ?>>Creepster</option>
					<option value="Crete Round"  <?php selected($lk_font_style, 'Crete Round' ); ?>>Crete Round</option>
					<option value="Crimson Text"  <?php selected($lk_font_style, 'Crimson Text' ); ?>>Crimson Text</option>
					<option value="Crushed"  <?php selected($lk_font_style, 'Crushed' ); ?>>Crushed</option>
					<option value="Cuprum"  <?php selected($lk_font_style, 'Cuprum' ); ?>>Cuprum</option>
					<option value="Cutive"  <?php selected($lk_font_style, 'Cutive' ); ?>>Cutive</option>
					<option value="Damion"  <?php selected($lk_font_style, 'Damion' ); ?>>Damion</option>
					<option value="Dancing Script"  <?php selected($lk_font_style, 'Dancing Script' ); ?>>Dancing Script</option>
					<option value="Dangrek"  <?php selected($lk_font_style, 'Dangrek' ); ?>>Dangrek</option>
					<option value="Dawning of a New Day"  <?php selected($lk_font_style, 'Dawning of a New Day' ); ?>>Dawning of a New Day</option>
					<option value="Days One"  <?php selected($lk_font_style, 'Days One' ); ?>>Days One</option>
					<option value="Delius"  <?php selected($lk_font_style, 'Delius' ); ?>>Delius</option>
					<option value="Delius Swash Caps"  <?php selected($lk_font_style, 'Delius Swash Caps' ); ?>>Delius Swash Caps</option>
					<option value="Delius Unicase"  <?php selected($lk_font_style, 'Delius Unicase' ); ?>>Delius Unicase</option>
					<option value="Della Respira"  <?php selected($lk_font_style, 'Della Respira' ); ?>>Della Respira</option>
					<option value="Devonshire"  <?php selected($lk_font_style, 'Devonshire' ); ?>>Devonshire</option>
					<option value="Didact Gothic"  <?php selected($lk_font_style, 'Didact Gothic' ); ?>>Didact Gothic</option>
					<option value="Diplomata"  <?php selected($lk_font_style, 'Diplomata' ); ?>>Diplomata</option>
					<option value="Diplomata SC"  <?php selected($lk_font_style, 'Diplomata SC' ); ?>>Diplomata SC</option>
					<option value="Doppio One"  <?php selected($lk_font_style, 'Doppio One' ); ?>>Doppio One</option>
					<option value="Dorsa"  <?php selected($lk_font_style, 'Dorsa' ); ?>>Dorsa</option>
					<option value="Dosis"  <?php selected($lk_font_style, 'Dosis' ); ?>>Dosis</option>
					<option value="Dr Sugiyama"  <?php selected($lk_font_style, 'Dr Sugiyama' ); ?>>Dr Sugiyama</option>
					<option value="Droid Sans"  <?php selected($lk_font_style, 'Droid Sans' ); ?>>Droid Sans</option>
					<option value="Droid Sans Mono"  <?php selected($lk_font_style, 'Droid Sans Mono' ); ?>>Droid Sans Mono</option>
					<option value="Droid Serif" <?php selected($lk_font_style, 'Droid Serif' ); ?>>Droid Serif</option>
					<option value="Duru Sans" <?php selected($lk_font_style, 'Duru Sans' ); ?>>Duru Sans</option>
					<option value="Dynalight" <?php selected($lk_font_style, 'Dynalight' ); ?>>Dynalight</option>
					<option value="EB Garamond" <?php selected($lk_font_style, 'EB Garamond' ); ?>>EB Garamond</option>
					<option value="Eater" <?php selected($lk_font_style, 'Eater' ); ?>>Eater</option>
					<option value="Economica" <?php selected($lk_font_style, 'Economica' ); ?>>Economica</option>
					<option value="Electrolize" <?php selected($lk_font_style, 'Electrolize' ); ?>>Electrolize</option>
					<option value="Emblema One" <?php selected($lk_font_style, 'Emblema One' ); ?>>Emblema One</option>
					<option value="Emilys Candy" <?php selected($lk_font_style, 'Emilys Candy' ); ?>>Emilys Candy</option>
					<option value="Engagement" <?php selected($lk_font_style, 'Engagement' ); ?>>Engagement</option>
					<option value="Enriqueta" <?php selected($lk_font_style, 'Enriqueta' ); ?>>Enriqueta</option>
					<option value="Erica One" <?php selected($lk_font_style, 'Erica One' ); ?>>Erica One</option>
					<option value="Esteban" <?php selected($lk_font_style, 'Esteban' ); ?>>Esteban</option>
					<option value="Euphoria Script">Euphoria Script</option>
					<option value="Ewert" <?php selected($lk_font_style, 'Ewert' ); ?>>Ewert</option>
					<option value="Exo" <?php selected($lk_font_style, 'Exo' ); ?>>Exo</option>
					<option value="Expletus Sans" <?php selected($lk_font_style, 'Expletus Sans' ); ?>>Expletus Sans</option>
					<option value="Fanwood Text" <?php selected($lk_font_style, 'Fanwood Text' ); ?>>Fanwood Text</option>
					<option value="Fascinate" <?php selected($lk_font_style, 'Fascinate' ); ?>>Fascinate</option>
					<option value="Fascinate Inline" <?php selected($lk_font_style, 'Fascinate Inline' ); ?>>Fascinate Inline</option>
					<option value="Federant" <?php selected($lk_font_style, 'Federant' ); ?>>Federant</option>
					<option value="Federo" <?php selected($lk_font_style, 'Federo' ); ?>>Federo</option>
					<option value="Felipa" <?php selected($lk_font_style, 'Felipa' ); ?>>Felipa</option>
					<option value="Fjord One" <?php selected($lk_font_style, 'Fjord One' ); ?>>Fjord One</option>
					<option value="Flamenco" <?php selected($lk_font_style, 'Flamenco' ); ?>>Flamenco</option>
					<option value="Flavors" <?php selected($lk_font_style, 'Flavors' ); ?>>Flavors</option>
					<option value="Fondamento" <?php selected($lk_font_style, 'Fondamento' ); ?>>Fondamento</option>
					<option value="Fontdiner Swanky" <?php selected($lk_font_style, 'Fontdiner Swanky' ); ?>>Fontdiner Swanky</option>
					<option value="Forum" <?php selected($lk_font_style, 'Forum' ); ?>>Forum</option>
					<option value="Francois One" <?php selected($lk_font_style, 'Francois One' ); ?>>Francois One</option>
					<option value="Fredericka the Great" <?php selected($lk_font_style, 'Fredericka the Great' ); ?>>Fredericka the Great</option>
					<option value="Fredoka One" <?php selected($lk_font_style, 'Fredoka One' ); ?>>Fredoka One</option>
					<option value="Freehand" <?php selected($lk_font_style, 'Freehand' ); ?>>Freehand</option>
					<option value="Fresca" <?php selected($lk_font_style, 'Fresca' ); ?>>Fresca</option>
					<option value="Frijole" <?php selected($lk_font_style, 'Frijole' ); ?>>Frijole</option>
					<option value="Fugaz One" <?php selected($lk_font_style, 'Fugaz One' ); ?>>Fugaz One</option>
					<option value="GFS Didot" <?php selected($lk_font_style, 'GFS Didot' ); ?>>GFS Didot</option>
					<option value="GFS Neohellenic" <?php selected($lk_font_style, 'GFS Neohellenic' ); ?>>GFS Neohellenic</option>
					<option value="Galdeano" <?php selected($lk_font_style, 'Galdeano' ); ?>>Galdeano</option>
					<option value="Gentium Basic" <?php selected($lk_font_style, 'Gentium Basic' ); ?>>Gentium Basic</option>
					<option value="Gentium Book Basic" <?php selected($lk_font_style, 'Gentium Book Basic' ); ?>>Gentium Book Basic</option>
					<option value="Geo" <?php selected($lk_font_style, 'Geo' ); ?>>Geo</option>
					<option value="Geostar" <?php selected($lk_font_style, 'Geostar' ); ?>>Geostar</option>
					<option value="Geostar Fill" <?php selected($lk_font_style, 'Geostar Fill' ); ?>>Geostar Fill</option>
					<option value="Germania One" <?php selected($lk_font_style, 'Germania One' ); ?>>Germania One</option>
					<option value="Give You Glory" <?php selected($lk_font_style, 'Give You Glory' ); ?>>Give You Glory</option>
					<option value="Glass Antiqua" <?php selected($lk_font_style, 'Glass Antiqua' ); ?>>Glass Antiqua</option>
					<option value="Glegoo" <?php selected($lk_font_style, 'Glegoo' ); ?>>Glegoo</option>
					<option value="Gloria Hallelujah" <?php selected($lk_font_style, 'Gloria Hallelujah' ); ?>>Gloria Hallelujah</option>
					<option value="Goblin One" <?php selected($lk_font_style, 'Goblin One' ); ?>>Goblin One</option>
					<option value="Gochi Hand" <?php selected($lk_font_style, 'Gochi Hand' ); ?>>Gochi Hand</option>
					<option value="Gorditas" <?php selected($lk_font_style, 'Gorditas' ); ?>>Gorditas</option>
					<option value="Goudy Bookletter 1911" <?php selected($lk_font_style, 'Goudy Bookletter 191' ); ?>>Goudy Bookletter 1911</option>
					<option value="Graduate" <?php selected($lk_font_style, 'Graduate' ); ?>>Graduate</option>
					<option value="Gravitas One" <?php selected($lk_font_style, 'Gravitas One' ); ?>>Gravitas One</option>
					<option value="Great Vibes" <?php selected($lk_font_style, 'Great Vibes' ); ?>>Great Vibes</option>
					<option value="Gruppo" <?php selected($lk_font_style, 'Gruppo' ); ?>>Gruppo</option>
					<option value="Gudea" <?php selected($lk_font_style, 'Gudea' ); ?>>Gudea</option>
					<option value="Habibi" <?php selected($lk_font_style, 'Habibi' ); ?>>Habibi</option>
					<option value="Hammersmith One" <?php selected($lk_font_style, 'Hammersmith One' ); ?>>Hammersmith One</option>
					<option value="Handlee" <?php selected($lk_font_style, 'Handlee' ); ?>>Handlee</option>
					<option value="Hanuman" <?php selected($lk_font_style, 'Hanuman' ); ?>>Hanuman</option>
					<option value="Happy Monkey" <?php selected($lk_font_style, 'Happy Monkey' ); ?>>Happy Monkey</option>
					<option value="Henny Penny" <?php selected($lk_font_style, 'Henny Penny' ); ?>>Henny Penny</option>
					<option value="Herr Von Muellerhoff" <?php selected($lk_font_style, 'Herr Von Muellerhoff' ); ?>>Herr Von Muellerhoff</option>
					<option value="Holtwood One SC" <?php selected($lk_font_style, 'Holtwood One SC' ); ?>>Holtwood One SC</option>
					<option value="Homemade Apple" <?php selected($lk_font_style, 'Homemade Apple' ); ?>>Homemade Apple</option>
					<option value="Homenaje" <?php selected($lk_font_style, 'Homenaje' ); ?>>Homenaje</option>
					<option value="IM Fell DW Pica" <?php selected($lk_font_style, 'IM Fell DW Pica' ); ?>>IM Fell DW Pica</option>
					<option value="IM Fell DW Pica SC" <?php selected($lk_font_style, 'IM Fell DW Pica SC' ); ?>>IM Fell DW Pica SC</option>
					<option value="IM Fell Double Pica" <?php selected($lk_font_style, 'IM Fell Double Pica' ); ?>>IM Fell Double Pica</option>
					<option value="IM Fell Double Pica SC" <?php selected($lk_font_style, 'IM Fell Double Pica SC' ); ?>>IM Fell Double Pica SC</option>
					<option value="IM Fell English" <?php selected($lk_font_style, 'IM Fell English' ); ?>>IM Fell English</option>
					<option value="IM Fell English SC" <?php selected($lk_font_style, 'IM Fell English SC' ); ?>>IM Fell English SC</option>
					<option value="IM Fell French Canon" <?php selected($lk_font_style, 'IM Fell French Canon' ); ?>>IM Fell French Canon</option>
					<option value="IM Fell French Canon SC" <?php selected($lk_font_style, 'IM Fell French Canon SC' ); ?>>IM Fell French Canon SC</option>
					<option value="IM Fell Great Primer" <?php selected($lk_font_style, 'IM Fell Great Primer' ); ?>>IM Fell Great Primer</option>
					<option value="IM Fell Great Primer SC" <?php selected($lk_font_style, 'IM Fell Great Primer SC' ); ?>>IM Fell Great Primer SC</option>
					<option value="Iceberg" <?php selected($lk_font_style, 'Iceberg' ); ?>>Iceberg</option>
					<option value="Iceland" <?php selected($lk_font_style, 'Iceland' ); ?>>Iceland</option>
					<option value="Imprima" <?php selected($lk_font_style, 'Imprima' ); ?>>Imprima</option>
					<option value="Inconsolata" <?php selected($lk_font_style, 'Inconsolata' ); ?>>Inconsolata</option>
					<option value="Inder" <?php selected($lk_font_style, 'Inder' ); ?>>Inder</option>
					<option value="Indie Flower" <?php selected($lk_font_style, 'Indie Flower' ); ?>>Indie Flower</option>
					<option value="Inika" <?php selected($lk_font_style, 'Inika' ); ?>>Inika</option>
					<option value="Irish Grover" <?php selected($lk_font_style, 'Irish Grover' ); ?>>Irish Grover</option>
					<option value="Istok Web" <?php selected($lk_font_style, 'Istok Web' ); ?>>Istok Web</option>
					<option value="Italiana" <?php selected($lk_font_style, 'Italiana' ); ?>>Italiana</option>
					<option value="Italianno" <?php selected($lk_font_style, 'Italianno' ); ?>>Italianno</option>
					<option value="Jim Nightshade" <?php selected($lk_font_style, 'Jim Nightshade' ); ?>>Jim Nightshade</option>
					<option value="Jockey One" <?php selected($lk_font_style, 'Jockey One' ); ?>>Jockey One</option>
					<option value="Jolly Lodger" <?php selected($lk_font_style, 'Jolly Lodger' ); ?>>Jolly Lodger</option>
					<option value="Josefin Sans" <?php selected($lk_font_style, 'Josefin Sans' ); ?>>Josefin Sans</option>
					<option value="Josefin Slab" <?php selected($lk_font_style, 'Josefin Slab' ); ?>>Josefin Slab</option>
					<option value="Judson" <?php selected($lk_font_style, 'Judson' ); ?>>Judson</option>
					<option value="Julee" <?php selected($lk_font_style, 'Julee' ); ?>>Julee</option>
					<option value="Junge" <?php selected($lk_font_style, 'Junge' ); ?>>Junge</option>
					<option value="Jura" <?php selected($lk_font_style, 'Jura' ); ?>>Jura</option>
					<option value="Just Another Hand" <?php selected($lk_font_style, 'Just Another Hand' ); ?>>Just Another Hand</option>
					<option value="Just Me Again Down Here" <?php selected($lk_font_style, 'Just Me Again Down Here' ); ?>>Just Me Again Down Here</option>
					<option value="Kameron" <?php selected($lk_font_style, 'Kameron' ); ?>>Kameron</option>
					<option value="Karla" <?php selected($lk_font_style, 'Karla' ); ?>>Karla</option>
					<option value="Kaushan Script" <?php selected($lk_font_style, 'Kaushan Script' ); ?>>Kaushan Script</option>
					<option value="Kelly Slab" <?php selected($lk_font_style, 'Kelly Slab' ); ?>>Kelly Slab</option>
					<option value="Kenia" <?php selected($lk_font_style, 'Kenia' ); ?>>Kenia</option>
					<option value="Khmer" <?php selected($lk_font_style, 'Khmer' ); ?>>Khmer</option>
					<option value="Knewave" <?php selected($lk_font_style, 'Knewave' ); ?>>Knewave</option>
					<option value="Kotta One" <?php selected($lk_font_style, 'Kotta One' ); ?>>Kotta One</option>
					<option value="Koulen" <?php selected($lk_font_style, 'Koulen' ); ?>>Koulen</option>
					<option value="Kranky" <?php selected($lk_font_style, 'Kranky' ); ?>>Kranky</option>
					<option value="Kreon" <?php selected($lk_font_style, 'Kreon' ); ?>>Kreon</option>
					<option value="Kristi" <?php selected($lk_font_style, 'Kristi' ); ?>>Kristi</option>
					<option value="Krona One" <?php selected($lk_font_style, 'Krona One' ); ?>>Krona One</option>
					<option value="La Belle Aurore" <?php selected($lk_font_style, 'La Belle Aurore' ); ?>>La Belle Aurore</option>
					<option value="Lancelot" <?php selected($lk_font_style, 'Lancelot' ); ?>>Lancelot</option>
					<option value="Lato" <?php selected($lk_font_style, 'Lato' ); ?>>Lato</option>
					<option value="League Script" <?php selected($lk_font_style, 'League Script' ); ?>>League Script</option>
					<option value="Leckerli One" <?php selected($lk_font_style, 'Leckerli One' ); ?>>Leckerli One</option>
					<option value="Ledger" <?php selected($lk_font_style, 'Ledger' ); ?>>Ledger</option>
					<option value="Lekton" <?php selected($lk_font_style, 'Lekton' ); ?>>Lekton</option>
					<option value="Lemon" <?php selected($lk_font_style, 'Lemon' ); ?>>Lemon</option>
					<option value="Lilita One" <?php selected($lk_font_style, 'Lilita One' ); ?>>Lilita One</option>
					<option value="Limelight" <?php selected($lk_font_style, 'Limelight' ); ?>>Limelight</option>
					<option value="Linden Hill" <?php selected($lk_font_style, 'Linden Hill' ); ?>>Linden Hill</option>
					<option value="Lobster" <?php selected($lk_font_style, 'Lobster' ); ?>>Lobster</option>
					<option value="Lobster Two" <?php selected($lk_font_style, 'Lobster Two' ); ?>>Lobster Two</option>
					<option value="Londrina Outline" <?php selected($lk_font_style, 'Londrina Outline' ); ?>>Londrina Outline</option>
					<option value="Londrina Shadow" <?php selected($lk_font_style, 'Londrina Shadow' ); ?>>Londrina Shadow</option>
					<option value="Londrina Sketch" <?php selected($lk_font_style, 'Londrina Sketch' ); ?>>Londrina Sketch</option>
					<option value="Londrina Solid" <?php selected($lk_font_style, 'Londrina Solid' ); ?>>Londrina Solid</option>
					<option value="Lora" <?php selected($lk_font_style, 'Lora' ); ?>>Lora</option>
					<option value="Love Ya Like A Sister" <?php selected($lk_font_style, 'Love Ya Like A Sister' ); ?>>Love Ya Like A Sister</option>
					<option value="Loved by the King" <?php selected($lk_font_style, 'Loved by the King' ); ?>>Loved by the King</option>
					<option value="Lovers Quarrel" <?php selected($lk_font_style, 'Lovers Quarrel' ); ?>>Lovers Quarrel</option>
					<option value="Luckiest Guy" <?php selected($lk_font_style, 'Luckiest Guy' ); ?>>Luckiest Guy</option>
					<option value="Lusitana" <?php selected($lk_font_style, 'Lusitana' ); ?>>Lusitana</option>
					<option value="Lustria" <?php selected($lk_font_style, 'Lustria' ); ?>>Lustria</option>
					<option value="Macondo" <?php selected($lk_font_style, 'Macondo' ); ?>>Macondo</option>
					<option value="Macondo Swash Caps" <?php selected($lk_font_style, 'Macondo Swash Caps' ); ?>>Macondo Swash Caps</option>
					<option value="Magra" <?php selected($lk_font_style, 'Magra' ); ?>>Magra</option>
					<option value="Maiden Orange" <?php selected($lk_font_style, 'Maiden Orange' ); ?>>Maiden Orange</option>
					<option value="Mako" <?php selected($lk_font_style, 'Mako' ); ?>>Mako</option>
					<option value="Marck Script" <?php selected($lk_font_style, 'Marck Script' ); ?>>Marck Script</option>
					<option value="Marko One" <?php selected($lk_font_style, 'Marko One' ); ?>>Marko One</option>
					<option value="Marmelad" <?php selected($lk_font_style, 'Marmelad' ); ?>>Marmelad</option>
					<option value="Marvel" <?php selected($lk_font_style, 'Marvel' ); ?>>Marvel</option>
					<option value="Mate" <?php selected($lk_font_style, 'Mate' ); ?>>Mate</option>
					<option value="Mate SC" <?php selected($lk_font_style, 'Mate SC' ); ?>>Mate SC</option>
					<option value="Maven Pro" <?php selected($lk_font_style, 'Maven Pro' ); ?>>Maven Pro</option>
					<option value="Meddon" <?php selected($lk_font_style, 'Meddon' ); ?>>Meddon</option>
					<option value="MedievalSharp" <?php selected($lk_font_style, 'MedievalSharp' ); ?>>MedievalSharp</option>
					<option value="Medula One" <?php selected($lk_font_style, 'Medula One' ); ?>>Medula One</option>
					<option value="Megrim" <?php selected($lk_font_style, 'Megrim' ); ?>>Megrim</option>
					<option value="Merienda One" <?php selected($lk_font_style, 'Merienda One' ); ?>>Merienda One</option>
					<option value="Merriweather" <?php selected($lk_font_style, 'Merriweather' ); ?>>Merriweather</option>
					<option value="Metal" <?php selected($lk_font_style, 'Metal' ); ?>>Metal</option>
					<option value="Metamorphous"<?php selected($lk_font_style, 'Metamorphous' ); ?>>Metamorphous</option>
					<option value="Metrophobic" <?php selected($lk_font_style, 'Metrophobic' ); ?>>Metrophobic</option>
					<option value="Michroma" <?php selected($lk_font_style, 'Michroma' ); ?>>Michroma</option>
					<option value="Miltonian" <?php selected($lk_font_style, 'Miltonian' ); ?>>Miltonian</option>
					<option value="Miltonian Tattoo" <?php selected($lk_font_style, 'Miltonian Tattoo' ); ?>>Miltonian Tattoo</option>
					<option value="Miniver" <?php selected($lk_font_style, 'Miniver' ); ?>>Miniver</option>
					<option value="Miss Fajardose" <?php selected($lk_font_style, 'Miss Fajardose' ); ?>>Miss Fajardose</option>
					<option value="Modern Antiqua" <?php selected($lk_font_style, 'Modern Antiqua' ); ?>>Modern Antiqua</option>
					<option value="Molengo" <?php selected($lk_font_style, 'Molengo' ); ?>>Molengo</option>
					<option value="Monofett" <?php selected($lk_font_style, 'Monofett' ); ?>>Monofett</option>
					<option value="Monoton" <?php selected($lk_font_style, 'Monoton' ); ?>>Monoton</option>
					<option value="Monsieur La Doulaise" <?php selected($lk_font_style, 'Monsieur La Doulaise' ); ?>>Monsieur La Doulaise</option>
					<option value="Montaga" <?php selected($lk_font_style, 'Montaga' ); ?>>Montaga</option>
					<option value="Montez" <?php selected($lk_font_style, 'Montez' ); ?>>Montez</option>
					<option value="Montserrat" <?php selected($lk_font_style, 'Montserrat' ); ?>>Montserrat</option>
					<option value="Moul" <?php selected($lk_font_style, 'Moul' ); ?>>Moul</option>
					<option value="Moulpali" <?php selected($lk_font_style, 'Moulpali' ); ?>>Moulpali</option>
					<option value="Mountains of Christmas" <?php selected($lk_font_style, 'Mountains of Christmas' ); ?>>Mountains of Christmas</option>
					<option value="Mr Bedfort" <?php selected($lk_font_style, 'Mr Bedfort' ); ?>>Mr Bedfort</option>
					<option value="Mr Dafoe" <?php selected($lk_font_style, 'Mr Dafoe' ); ?>>Mr Dafoe</option>
					<option value="Mr De Haviland" <?php selected($lk_font_style, 'Mr De Haviland' ); ?>>Mr De Haviland</option>
					<option value="Mrs Saint Delafield" <?php selected($lk_font_style, 'Mrs Saint Delafield' ); ?>>Mrs Saint Delafield</option>
					<option value="Mrs Sheppards" <?php selected($lk_font_style, 'Mrs Sheppards' ); ?>>Mrs Sheppards</option>
					<option value="Muli" <?php selected($lk_font_style, 'Muli' ); ?>>Muli</option>
					<option value="Mystery Quest" <?php selected($lk_font_style, 'Mystery Quest' ); ?>>Mystery Quest</option>
					<option value="Neucha" <?php selected($lk_font_style, 'Neucha' ); ?>>Neucha</option>
					<option value="Neuton" <?php selected($lk_font_style, 'Neuton' ); ?>>Neuton</option>
					<option value="News Cycle" <?php selected($lk_font_style, 'News Cycle' ); ?>>News Cycle</option>
					<option value="Niconne" <?php selected($lk_font_style, 'Niconne' ); ?>>Niconne</option>
					<option value="Nixie One" <?php selected($lk_font_style, 'Nixie One' ); ?>>Nixie One</option>
					<option value="Nobile" <?php selected($lk_font_style, 'Nobile' ); ?>>Nobile</option>
					<option value="Nokora" <?php selected($lk_font_style, 'Nokora' ); ?>>Nokora</option>
					<option value="Norican" <?php selected($lk_font_style, 'Norican' ); ?>>Norican</option>
					<option value="Nosifer" <?php selected($lk_font_style, 'Nosifer' ); ?>>Nosifer</option>
					<option value="Nothing You Could Do" <?php selected($lk_font_style, 'Nothing You Could Do' ); ?>>Nothing You Could Do</option>
					<option value="Noticia Text" <?php selected($lk_font_style, 'Noticia Text' ); ?>>Noticia Text</option>
					<option value="Nova Cut" <?php selected($lk_font_style, 'Nova Cut' ); ?>>Nova Cut</option>
					<option value="Nova Flat" <?php selected($lk_font_style, 'Nova Flat' ); ?>>Nova Flat</option>
					<option value="Nova Mono" <?php selected($lk_font_style, 'Nova Mono' ); ?>>Nova Mono</option>
					<option value="Nova Oval" <?php selected($lk_font_style, 'Nova Oval' ); ?>>Nova Oval</option>
					<option value="Nova Round" <?php selected($lk_font_style, 'Nova Round' ); ?>>Nova Round</option>
					<option value="Nova Script" <?php selected($lk_font_style, 'Nova Script' ); ?>>Nova Script</option>
					<option value="Nova Slim" <?php selected($lk_font_style, 'Nova Slim' ); ?>>Nova Slim</option>
					<option value="Nova Square" <?php selected($lk_font_style, 'Nova Square' ); ?>>Nova Square</option>
					<option value="Numans" <?php selected($lk_font_style, 'Numans' ); ?>>Numans</option>

					<option value="Nunito" <?php selected($lk_font_style, 'Nunito' ); ?>>Nunito</option>
					<option value="Odor Mean Chey" <?php selected($lk_font_style, 'Odor Mean Chey' ); ?>>Odor Mean Chey</option>
					<option value="Old Standard TT" <?php selected($lk_font_style, 'Old Standard TT' ); ?>>Old Standard TT</option>
					<option value="Oldenburg" <?php selected($lk_font_style, 'Oldenburg' ); ?>>Oldenburg</option>
					<option value="Oleo Script" <?php selected($lk_font_style, 'Oleo Script' ); ?>>Oleo Script</option>
					<option value="Open Sans" <?php selected($lk_font_style, 'Open Sans' ); ?>>Open Sans</option>
					<option value="Open Sans Condensed" <?php selected($lk_font_style, 'Open Sans Condensed' ); ?>>Open Sans Condensed</option>
					<option value="Orbitron" <?php selected($lk_font_style, 'Orbitron' ); ?>>Orbitron</option>
					<option value="Original Surfer" <?php selected($lk_font_style, 'Original Surfer' ); ?>>Original Surfer</option>
					<option value="Oswald" <?php selected($lk_font_style, 'Oswald' ); ?>>Oswald</option>
					<option value="Over the Rainbow" <?php selected($lk_font_style, 'Over the Rainbow' ); ?>>Over the Rainbow</option>
					<option value="Overlock" <?php selected($lk_font_style, 'Overlock' ); ?>>Overlock</option>
					<option value="Overlock SC" <?php selected($lk_font_style, 'Overlock SC' ); ?>>Overlock SC</option>
					<option value="Ovo" <?php selected($lk_font_style, 'Ovo' ); ?>>Ovo</option>
					<option value="Oxygen" <?php selected($lk_font_style, 'Oxygen' ); ?>>Oxygen</option>
					<option value="PT Mono" <?php selected($lk_font_style, 'PT Mono' ); ?>>PT Mono</option>
					<option value="PT Sans" <?php selected($lk_font_style, 'PT Sans' ); ?>>PT Sans</option>
					<option value="PT Sans Caption" <?php selected($lk_font_style, 'PT Sans Caption' ); ?>>PT Sans Caption</option>
					<option value="PT Sans Narrow" <?php selected($lk_font_style, 'PT Sans Narrow' ); ?>>PT Sans Narrow</option>
					<option value="PT Serif" <?php selected($lk_font_style, 'PT Serif' ); ?>>PT Serif</option>
					<option value="PT Serif Caption" <?php selected($lk_font_style, 'PT Serif Caption' ); ?>>PT Serif Caption</option>
					<option value="Pacifico" <?php selected($lk_font_style, 'Pacifico' ); ?>>Pacifico</option>
					<option value="Parisienne" <?php selected($lk_font_style, 'Parisienne' ); ?>>Parisienne</option>
					<option value="Passero One" <?php selected($lk_font_style, 'Passero One' ); ?>>Passero One</option>
					<option value="Passion One" <?php selected($lk_font_style, 'Passion One' ); ?>>Passion One</option>
					<option value="Patrick Hand" <?php selected($lk_font_style, 'Patrick Hand' ); ?>>Patrick Hand</option>
					<option value="Patua One" <?php selected($lk_font_style, 'Patua One' ); ?>>Patua One</option>
					<option value="Paytone One" <?php selected($lk_font_style, 'Paytone One' ); ?>>Paytone One</option>
					<option value="Permanent Marker" <?php selected($lk_font_style, 'Permanent Marker' ); ?>>Permanent Marker</option>
					<option value="Petrona" <?php selected($lk_font_style, 'Petrona' ); ?>>Petrona</option>
					<option value="Philosopher" <?php selected($lk_font_style, 'Philosopher' ); ?>>Philosopher</option>
					<option value="Piedra" <?php selected($lk_font_style, 'Piedra' ); ?>>Piedra</option>
					<option value="Pinyon Script" <?php selected($lk_font_style, 'Pinyon Script' ); ?>>Pinyon Script</option>
					<option value="Plaster" <?php selected($lk_font_style, 'Plaster' ); ?>>Plaster</option>
					<option value="Play" <?php selected($lk_font_style, 'Play' ); ?>>Play</option>
					<option value="Playball" <?php selected($lk_font_style, 'Playball' ); ?>>Playball</option>
					<option value="Playfair Display" <?php selected($lk_font_style, 'Playfair Display' ); ?>>Playfair Display</option>
					<option value="Podkova" <?php selected($lk_font_style, 'Podkova' ); ?>>Podkova</option>
					<option value="Poiret One" <?php selected($lk_font_style, 'Poiret One' ); ?>>Poiret One</option>
					<option value="Poller One" <?php selected($lk_font_style, 'Poller One' ); ?>>Poller One</option>
					<option value="Poly" <?php selected($lk_font_style, 'Poly' ); ?>>Poly</option>
					<option value="Pompiere" <?php selected($lk_font_style, 'Pompiere' ); ?>>Pompiere</option>
					<option value="Pontano Sans" <?php selected($lk_font_style, 'Pontano Sans' ); ?>>Pontano Sans</option>
					<option value="Port Lligat Sans" <?php selected($lk_font_style, 'Port Lligat Sans' ); ?>>Port Lligat Sans</option>
					<option value="Port Lligat Slab" <?php selected($lk_font_style, 'Port Lligat Slab' ); ?>>Port Lligat Slab</option>
					<option value="Prata" <?php selected($lk_font_style, 'Prata' ); ?>>Prata</option>
					<option value="Preahvihear" <?php selected($lk_font_style, 'Preahvihear' ); ?>>Preahvihear</option>
					<option value="Press Start 2P" <?php selected($lk_font_style, 'Press Start 2P' ); ?>>Press Start 2P</option>
					<option value="Princess Sofia" <?php selected($lk_font_style, 'Princess Sofia' ); ?>>Princess Sofia</option>
					<option value="Prociono" <?php selected($lk_font_style, 'Prociono' ); ?>>Prociono</option>
					<option value="Prosto One" <?php selected($lk_font_style, 'Prosto One' ); ?>>Prosto One</option>
					<option value="Puritan" <?php selected($lk_font_style, 'Puritan' ); ?>>Puritan</option>
					<option value="Quantico" <?php selected($lk_font_style, 'Quantico' ); ?>>Quantico</option>
					<option value="Quattrocento" <?php selected($lk_font_style, 'Quattrocento' ); ?>>Quattrocento</option>
					<option value="Quattrocento Sans" <?php selected($lk_font_style, 'Quattrocento Sans' ); ?>>Quattrocento Sans</option>
					<option value="Questrial" <?php selected($lk_font_style, 'Questrial' ); ?>>Questrial</option>
					<option value="Quicksand" <?php selected($lk_font_style, 'Quicksand' ); ?>>Quicksand</option>
					<option value="Qwigley" <?php selected($lk_font_style, 'Qwigley' ); ?>>Qwigley</option>
					<option value="Radley" <?php selected($lk_font_style, 'Radley' ); ?>>Radley</option>
					<option value="Raleway" <?php selected($lk_font_style, 'Raleway' ); ?>>Raleway</option>
					<option value="Rammetto One" <?php selected($lk_font_style, 'Rammetto One' ); ?>>Rammetto One</option>
					<option value="Rancho" <?php selected($lk_font_style, 'Rancho' ); ?>>Rancho</option>
					<option value="Rationale" <?php selected($lk_font_style, 'Rationale' ); ?>>Rationale</option>
					<option value="Redressed" <?php selected($lk_font_style, 'Redressed' ); ?>>Redressed</option>
					<option value="Reenie Beanie" <?php selected($lk_font_style, 'Reenie Beanie' ); ?>>Reenie Beanie</option>
					<option value="Revalia" <?php selected($lk_font_style, 'Revalia' ); ?>>Revalia</option>
					<option value="Ribeye" <?php selected($lk_font_style, 'Ribeye' ); ?>>Ribeye</option>
					<option value="Ribeye Marrow" <?php selected($lk_font_style, 'Ribeye Marrow' ); ?>>Ribeye Marrow</option>
					<option value="Righteous" <?php selected($lk_font_style, 'Righteous' ); ?>>Righteous</option>
					<option value="Rochester" <?php selected($lk_font_style, 'Rochester' ); ?>>Rochester</option>
					<option value="Rock Salt" <?php selected($lk_font_style, 'Rock Salt' ); ?>>Rock Salt</option>
					<option value="Rokkitt" <?php selected($lk_font_style, 'Rokkitt' ); ?>>Rokkitt</option>
					<option value="Ropa Sans" <?php selected($lk_font_style, 'Ropa Sans' ); ?>>Ropa Sans</option>
					<option value="Rosario" <?php selected($lk_font_style, 'Rosario' ); ?>>Rosario</option>
					<option value="Rosarivo" <?php selected($lk_font_style, 'Rosarivo' ); ?>>Rosarivo</option>
					<option value="Rouge Script" <?php selected($lk_font_style, 'Rouge Script' ); ?>>Rouge Script</option>
					<option value="Ruda" <?php selected($lk_font_style, 'Ruda' ); ?>>Ruda</option>
					<option value="Ruge Boogie" <?php selected($lk_font_style, 'Ruge Boogie' ); ?>>Ruge Boogie</option>
					<option value="Ruluko" <?php selected($lk_font_style, 'Ruluko' ); ?>>Ruluko</option>
					<option value="Ruslan Display" <?php selected($lk_font_style, 'Ruslan Display' ); ?>>Ruslan Display</option>
					<option value="Russo One" <?php selected($lk_font_style, 'Russo One' ); ?>>Russo One</option>
					<option value="Ruthie" <?php selected($lk_font_style, 'Ruthie' ); ?>>Ruthie</option>
					<option value="Sail" <?php selected($lk_font_style, 'Sail' ); ?>>Sail</option>
					<option value="Salsa" <?php selected($lk_font_style, 'Salsa' ); ?>>Salsa</option>
					<option value="Sancreek" <?php selected($lk_font_style, 'Sancreek' ); ?>>Sancreek</option>
					<option value="Sansita One" <?php selected($lk_font_style, 'Sansita One' ); ?>>Sansita One</option>
					<option value="Sarina" <?php selected($lk_font_style, 'Sarina' ); ?>>Sarina</option>
					<option value="Satisfy" <?php selected($lk_font_style, 'Satisfy' ); ?>>Satisfy</option>
					<option value="Schoolbell" <?php selected($lk_font_style, 'Schoolbell' ); ?>>Schoolbell</option>
					<option value="Seaweed Script" <?php selected($lk_font_style, 'Seaweed Script' ); ?>>Seaweed Script</option>
					<option value="Sevillana" <?php selected($lk_font_style, 'Sevillana' ); ?>>Sevillana</option>
					<option value="Shadows Into Light" <?php selected($lk_font_style, 'Shadows Into Light' ); ?>>Shadows Into Light</option>
					<option value="Shadows Into Light Two" <?php selected($lk_font_style, 'Shadows Into Light Two' ); ?>>Shadows Into Light Two</option>
					<option value="Shanti" <?php selected($lk_font_style, 'Shanti' ); ?>>Shanti</option>
					<option value="Share">Share</option>
					<option value="Shojumaru" <?php selected($lk_font_style, 'Shojumaru' ); ?>>Shojumaru</option>
					<option value="Short Stack" <?php selected($lk_font_style, 'Short Stack' ); ?>>Short Stack</option>
					<option value="Siemreap"<?php selected($lk_font_style, 'Siemreap' ); ?>>Siemreap</option>
					<option value="Sigmar One" <?php selected($lk_font_style, 'Sigmar One' ); ?>>Sigmar One</option>
					<option value="Signika"<?php selected($lk_font_style, 'Signika' ); ?>>Signika</option>
					<option value="Signika Negative" <?php selected($lk_font_style, 'Signika Negative' ); ?>>Signika Negative</option>
					<option value="Simonetta" <?php selected($lk_font_style, 'Simonetta' ); ?>>Simonetta</option>
					<option value="Sirin Stencil" <?php selected($lk_font_style, 'Sirin Stencil' ); ?>>Sirin Stencil</option>
					<option value="Six Caps" <?php selected($lk_font_style, 'Six Caps' ); ?>>Six Caps</option>
					<option value="Slackey" <?php selected($lk_font_style, 'Slackey' ); ?>>Slackey</option>
					<option value="Smokum" <?php selected($lk_font_style, 'Smokum' ); ?>>Smokum</option>
					<option value="Smythe" <?php selected($lk_font_style, 'Smythe' ); ?>>Smythe</option>
					<option value="Sniglet" <?php selected($lk_font_style, 'Sniglet' ); ?>>Sniglet</option>
					<option value="Snippet" <?php selected($lk_font_style, 'Snippet' ); ?>>Snippet</option>
					<option value="Sofia" <?php selected($lk_font_style, 'Sofia' ); ?>>Sofia</option>
					<option value="Sonsie One" <?php selected($lk_font_style, 'Sonsie One' ); ?>>Sonsie One</option>
					<option value="Sorts Mill Goudy" <?php selected($lk_font_style, 'Sorts Mill Goudy' ); ?>>Sorts Mill Goudy</option>
					<option value="Special Elite" <?php selected($lk_font_style, 'Special Elite' ); ?>>Special Elite</option>
					<option value="Spicy Rice" <?php selected($lk_font_style, 'Spicy Rice' ); ?>>Spicy Rice</option>
					<option value="Spinnaker" <?php selected($lk_font_style, 'Spinnaker' ); ?>>Spinnaker</option>
					<option value="Spirax" <?php selected($lk_font_style, 'Spirax' ); ?>>Spirax</option>
					<option value="Squada One" <?php selected($lk_font_style, 'Squada One' ); ?>>Squada One</option>
					<option value="Stardos Stencil" <?php selected($lk_font_style, 'Stardos Stencil' ); ?>>Stardos Stencil</option>
					<option value="Stint Ultra Condensed" <?php selected($lk_font_style, 'Stint Ultra Condensed' ); ?>>Stint Ultra Condensed</option>
					<option value="Stint Ultra Expanded" <?php selected($lk_font_style, 'Stint Ultra Expanded' ); ?>>Stint Ultra Expanded</option>
					<option value="Stoke" <?php selected($lk_font_style, 'Stoke' ); ?>>Stoke</option>
					<option value="Sue Ellen Francisco" <?php selected($lk_font_style, 'Sue Ellen Francisco' ); ?>>Sue Ellen Francisco</option>
					<option value="Sunshiney" <?php selected($lk_font_style, 'Sunshiney' ); ?>>Sunshiney</option>
					<option value="Supermercado One" <?php selected($lk_font_style, 'Supermercado One' ); ?>>Supermercado One</option>
					<option value="Suwannaphum" <?php selected($lk_font_style, 'Suwannaphum' ); ?>>Suwannaphum</option>
					<option value="Swanky and Moo Moo" <?php selected($lk_font_style, 'Swanky and Moo Moo' ); ?>>Swanky and Moo Moo</option>
					<option value="Syncopate" <?php selected($lk_font_style, 'Syncopate' ); ?>>Syncopate</option>
					<option value="Tangerine" <?php selected($lk_font_style, 'Tangerine' ); ?>>Tangerine</option>
					<option value="Taprom" <?php selected($lk_font_style, 'Taprom' ); ?>>Taprom</option>
					<option value="Telex" <?php selected($lk_font_style, 'Telex' ); ?>>Telex</option>
					<option value="Tenor Sans" <?php selected($lk_font_style, 'Tenor Sans' ); ?>>Tenor Sans</option>
					<option value="The Girl Next Door" <?php selected($lk_font_style, 'The Girl Next Door' ); ?>>The Girl Next Door</option>
					<option value="Tienne" <?php selected($lk_font_style, 'Tienne' ); ?>>Tienne</option>
					<option value="Tinos" <?php selected($lk_font_style, 'Tinos' ); ?>>Tinos</option>
					<option value="Titan One" <?php selected($lk_font_style, 'Titan One' ); ?>>Titan One</option>
					<option value="Trade Winds" <?php selected($lk_font_style, 'Trade Winds' ); ?> >Trade Winds</option>
					<option value="Trocchi" <?php selected($lk_font_style, 'Trocchi' ); ?>>Trocchi</option>
					<option value="Trochut" <?php selected($lk_font_style, 'Trochut' ); ?>>Trochut</option>
					<option value="Trykker" <?php selected($lk_font_style, 'Trykker' ); ?>>Trykker</option>
					<option value="Tulpen One" <?php selected($lk_font_style, 'Tulpen One' ); ?>>Tulpen One</option>
					<option value="Ubuntu" <?php selected($lk_font_style, 'Ubuntu' ); ?>>Ubuntu</option>
					<option value="Ubuntu Condensed" <?php selected($lk_font_style, 'Ubuntu Condensed' ); ?>>Ubuntu Condensed</option>
					<option value="Ubuntu Mono" <?php selected($lk_font_style, 'Ubuntu Mono' ); ?>>Ubuntu Mono</option>
					<option value="Ultra" <?php selected($lk_font_style, 'Ultra' ); ?>>Ultra</option>
					<option value="Uncial Antiqua" <?php selected($lk_font_style, 'Uncial Antiqua' ); ?>>Uncial Antiqua</option>
					<option value="UnifrakturCook" <?php selected($lk_font_style, 'UnifrakturCook' ); ?>>UnifrakturCook</option>
					<option value="UnifrakturMaguntia" <?php selected($lk_font_style, 'UnifrakturMaguntia' ); ?>>UnifrakturMaguntia</option>
					<option value="Unkempt" <?php selected($lk_font_style, 'Unkempt' ); ?>>Unkempt</option>
					<option value="Unlock" <?php selected($lk_font_style, 'Unlock' ); ?>>Unlock</option>
					<option value="Unna" <?php selected($lk_font_style, 'Unna' ); ?>>Unna</option>
					<option value="VT323" <?php selected($lk_font_style, 'VT323' ); ?>>VT323</option>
					<option value="Varela" <?php selected($lk_font_style, 'Varela' ); ?>>Varela</option>
					<option value="Varela Round" <?php selected($lk_font_style, 'Varela Round' ); ?>>Varela Round</option>
					<option value="Vast Shadow" <?php selected($lk_font_style, 'Vast Shadow' ); ?>>Vast Shadow</option>
					<option value="Vibur" <?php selected($lk_font_style, 'Vibur' ); ?>>Vibur</option>
					<option value="Vidaloka" <?php selected($lk_font_style, 'Vidaloka' ); ?>>Vidaloka</option>
					<option value="Viga" <?php selected($lk_font_style, 'Viga' ); ?>>Viga</option>
					<option value="Voces" <?php selected($lk_font_style, 'Voces' ); ?>>Voces</option>
					<option value="Volkhov" <?php selected($lk_font_style, 'Volkhov' ); ?>>Volkhov</option>
					<option value="Vollkorn" <?php selected($lk_font_style, 'Vollkorn' ); ?>>Vollkorn</option>
					<option value="Voltaire" <?php selected($lk_font_style, 'Voltaire' ); ?>>Voltaire</option>
					<option value="Waiting for the Sunrise" <?php selected($lk_font_style, 'Waiting for the Sunrise' ); ?>>Waiting for the Sunrise</option>
					<option value="Wallpoet" <?php selected($lk_font_style, 'Wallpoet' ); ?>>Wallpoet</option>
					<option value="Walter Turncoat" <?php selected($lk_font_style, 'Walter Turncoat' ); ?>>Walter Turncoat</option>
					<option value="Wellfleet" <?php selected($lk_font_style, 'Wellfleet' ); ?>>Wellfleet</option>
					<option value="Wire One" <?php selected($lk_font_style, 'Wire One' ); ?>>Wire One</option>
					<option value="Yanone Kaffeesatz" <?php selected($lk_font_style, 'Yanone Kaffeesatz' ); ?>>Yanone Kaffeesatz</option>
					<option value="Yellowtail" <?php selected($lk_font_style, 'Yellowtail' ); ?>>Yellowtail</option>
					<option value="Yeseva One" <?php selected($lk_font_style, 'Yeseva One' ); ?>>Yeseva One</option>
					<option value="Yesteryear" <?php selected($lk_font_style, 'Yesteryear' ); ?>>Yesteryear</option>
					<option value="Zeyada" <?php selected($lk_font_style, 'Zeyada' ); ?>>Zeyada</option>
				</optgroup>
			</select>
			<p class="description">
				Choose a caption font style.

			</p>

		</td>
	</tr>
	<tr>
		<th scope="row"><label>Button Title</label></th>
		<td>
			
			<input type="text" name="lk_button_title" id="lk_button_title" value="<?php echo $lk_button_title; ?>"/> 
			<button id="lk_btn_default_value">Default</button> 
			<p class="description">
				Write button title.
				
			</p>
		</td>
	</tr>		
	<tr>
		<th scope="row"><label>Light Box Styles</label></th>
		<td>
			
			<select name="lk_Light_Box" id="lk_Light_Box">
				<optgroup label="Select Light Box Styles">
					<option value="lightbox1" <?php if($lk_Light_Box == 'lightbox1') echo "selected=selected"; ?>>Nivo box</option>
					<option value="lightbox2" <?php if($lk_Light_Box == 'lightbox2') echo "selected=selected"; ?>>Pretty photo</option>
					<option value="lightbox3" <?php if($lk_Light_Box == 'lightbox3') echo "selected=selected"; ?>>Swipe Box</option>
					<option value="lightbox4" <?php if($lk_Light_Box == 'lightbox4') echo "selected=selected"; ?>>Fancy Box</option>

				</optgroup>
			</select>
			<p class="description">
				Choose an image Title style.
				
			</p>
		</td>
	</tr>
	
	<tr >
		<th scope="row"><label>Custom CSS</label></th>
		<td>
			<textarea id="lk_Custom_CSS" name="lk_Custom_CSS" type="text" class="" style="width:80%"><?php echo $lk_Custom_CSS; ?></textarea>
			<p class="description">
				Enter any custom css you want to apply on this gallery.<br>
				Note: Please Do Not Use <b>Style</b> Tag With Custom CSS.
			</p>
		</td>
	</tr>

</table>
<?php
?>
<script type="text/javascript">
jQuery(function(){
	jQuery("#lk_btn_default_value").click(function(e){
		e.preventDefault();
		jQuery("#lk_button_title").attr("value","Zoom");
	});
});
</script>