<?php 
/**
 * Plugin Name: Gallery Pro weblizar
 * Version: 5.0
 * Description: Design Photo, Video, Link, Image Gallery With Lightbox 
 * Author: Weblizar
 * Author URI: http://weblizar.com/plugins/photo-gallery-pluign/
 * Plugin URI: http://weblizar.com/plugins/photo-gallery-pluign/
 */

/**
 * Constant Variable
 */

define("WGP_PLUGIN_URL", plugin_dir_url(__FILE__));

register_activation_hook(__FILE__,'lk_DefaultSettingsPro');
function lk_DefaultSettingsPro(){
	$lk_defaultsetting=base64_encode(serialize(array(
		'wgp_Color'					=>"#e3e3e3",
		'lk_show_Gallery_title'		=>"Yes",
		'lk_show_image_label'		=>"Yes",
		'lk_show_gallery_layout'	=>"3",
		'lk_label_Color'			=>"#666",
		'lk_desc_font_Color'		=>"#777777",
		'lk_btn_Color'			    =>"#428bca",
		'lk_btn_font_Color'			=>"#FFF",
		'lk_font_style'				=>"Courgette",
		'lk_Custom_CSS'				=>"",
		'lk_show_img_desc'			=>"Yes",
		'lk_Light_Box'				=>"lightbox3",
		'lk_open_link'            	=>"_blank",
		'lk_button_title'			=>"Zoom"
		)));
	add_action("lksg_setting",$lk_defaultsetting);
}
/**
* Crop Images In Desire Format
*/

add_image_size('lksg_gallery_admin_thumb',300, 300, array( 'top', 'center' ));
add_image_size('lksg_gallery_admin_medium',400, 400, array( 'top', 'center' ));
add_image_size('lksg_gallery_admin_large',500, 500, array( 'top', 'center' ));
add_image_size('lksg_gallery_admin_medium_auto',400, 9999, array( 'top', 'center' ));
add_image_size('lksg_gallery_admin_large_auto',500, 9999, array( 'top', 'center' ));

add_action('admin_menu', 'submenu_SettingsPage');
function submenu_SettingsPage() {
	add_submenu_page('edit.php?post_type=acme_product', 'Pro Screenshots', 'Help and Support', 'administrator', 'lksg_gallery_pro', 'lksg_get_image_gallery_pro_page_function');

	add_submenu_page('edit.php?post_type=acme_product', 'Pro Screenshots', 'Our Products', 'administrator', 'lksg_gallery_pro_product', 'lksg_get_image_gallery_pro_product_function');


}

function lksg_get_image_gallery_pro_page_function()
{	
	require_once("gallery-pro-weblizar-help-and-support.php");
}

function lksg_get_image_gallery_pro_product_function()
{
	echo "products";
}

class lksg{
	private static $instance;
	private $admin_thumbnail_size = 150;
	private $thumbnail_size_w = 150;
	private $thumbnail_size_h = 150;
	var $counter;

	public static function forge() {
		if (!isset(self::$instance)) {
			$className = __CLASS__;
			self::$instance = new $className;
		}
		return self::$instance;
	}
	private function __construct() {
		$this->counter = 0;
		add_action('admin_enqueue_scripts', array(&$this,'my_style_files'));
		add_action('wp_enqueue_scripts', array(&$this,'frant_display_style'));
		if(is_admin())
		{
			add_action( 'init', array(&$this,'my_cpt'));
			add_action('wp_ajax_lksgallery_get_thumbnail', array(&$this,'lks_ajax_get_thumb'));

			add_action('add_meta_boxes',array(&$this, 'add_custom_meta_box'));
			add_action('save_post',array(&$this,'lksg_add_image_meta_box_save'));
			add_action('save_post',array(&$this,'lksg_settings_meta_save'));
			add_action('admin_init',array(&$this, 'add_custom_meta_box'));
		}
	}
	public function frant_display_style()
	{
		wp_enqueue_style('mystyle-5',WGP_PLUGIN_URL.'css/display_frant_css.css');
		wp_enqueue_style('mystyle-6',WGP_PLUGIN_URL.'css/bootstrap.css');				
		wp_enqueue_style('mystyle-8',WGP_PLUGIN_URL.'js/grid-folio/jquery.wm-gridfolio-1.0.min.css');
		wp_enqueue_script( 'jquery');	
		wp_enqueue_script('myscript-3',WGP_PLUGIN_URL.'js/grid-folio/jquery.wm-gridfolio-1.0.min.js');

		/**
             * Load Light Box 1 Swipebox JS CSS
        */   

		wp_enqueue_style('wl-lksg-swipe-css', WGP_PLUGIN_URL.'lightbox/swipebox/swipebox.css');
		wp_enqueue_script('wl-lksg-swipe-js',WGP_PLUGIN_URL.'lightbox/swipebox/jquery.swipebox.min.js');


		/**
             * Load Light Box 2  preety photo JS CSS
         */  
		wp_enqueue_style('lksg-pretty-css1', WGP_PLUGIN_URL.'lightbox/prettyphoto/lksg-prettyPhoto.css');
		wp_enqueue_script('lksg-pretty-js1', WGP_PLUGIN_URL.'lightbox/prettyphoto/lksg-jquery.prettyPhoto.js', array('jquery'));

		/**
             * Load Light Box 3 Nivobox JS CSS
        */
		wp_enqueue_style('lksg-nivo-lightbox-min-css',WGP_PLUGIN_URL.'lightbox/nivo/nivo-lightbox.min.css');
		wp_enqueue_script('lksg-nivo-lightbox-min-js_1',WGP_PLUGIN_URL.'lightbox/nivo/nivo-lightbox.min.js', array('jquery'));
		

		 /**
             * Load Light Box 4 Fancy JS CSS
          */
		 wp_enqueue_style('lksg-lbsp-fancybox-css', WGP_PLUGIN_URL.'lightbox/fancybox/jquery.fancybox.css');
		 wp_enqueue_script('lksg-lbsp-fancybox-js', WGP_PLUGIN_URL.'lightbox/fancybox/jquery.fancybox.js');
		 wp_enqueue_script('lksg-lbsp-fancybox-js1', WGP_PLUGIN_URL.'lightbox/fancybox/helpers/jquery.fancybox-media.js');

		 
		}
		public function my_style_files()
		{
			wp_enqueue_script( 'jquery');
			wp_enqueue_media();	
			wp_enqueue_script('media-upload');	
			wp_enqueue_style( 'wp-color-picker' );	
			wp_enqueue_style('mystyle-3',WGP_PLUGIN_URL.'css/font-awesome-latest/css/font-awesome.min.css');
			wp_enqueue_style('mystyle-4',WGP_PLUGIN_URL.'css/my_style.css');
			wp_enqueue_script( 'myscript-1',WGP_PLUGIN_URL.'js/my_script.js');			
			wp_enqueue_script( 'color-picker-script', WGP_PLUGIN_URL.'js/wl-color-picker.js', array( 'wp-color-picker' ), false, true );

		}
		public function add_custom_meta_box() 
		{
			add_meta_box('custom_meta_box', 'Photo Gallery shortcode',array(&$this, 'meta_box_function'),
				'acme_product','side','low');

			add_meta_box('add_img_box','Add Images',array(&$this,'add_image_meta_box'),'acme_product','normal','low');

			add_meta_box('gallery_setting','Apply Setting On Photo Gallery',array(&$this,'setting_meta_box_function'),'acme_product','normal','low');
		}
		public function meta_box_function() 
		{ 
			?>
			<p>Use below shortcode in any Page/Post to publish your gallery</p>
			<input  type="text" value="<?php echo "[WG id=".get_the_ID()."]"; ?>" readonly/>
			<?php 
		}


		public function setting_meta_box_function($post)
		{
			require_once('gallery-pro-weblizar-setting.php');

		}
		public function add_image_meta_box($post)
		{
			?>
			<div class="image_countainer">
				<input type="button" id="all_delete_btn" value="Delete All" rel="">
				<input type="hidden" id="lksg_wl_action" name="lksg_wl_action" value="lksg-save-settings">
				<ul class="clearfix" id="lksgallery_thumbs">
					<?php 
					$id=$post->ID;
					$lksg_AllPhotosDetails = unserialize(base64_decode(get_post_meta( $id, 'lksg_all_photos_details', true)));
					$TotalImg =  get_post_meta( $id, 'lksg_total_images_count', true );
				//print_r($lksg_AllPhotosDetails);

					if($TotalImg)
					{
						foreach($lksg_AllPhotosDetails as $data)
						{
							$name=$data['lksg_image_label'];
							$url_c=$data['lksg_image_url'];
							$lksg_img_thumb=$data['lksg_img_thumb'];
							$lksg_img_medium=$data['lksg_img_medium'];
							$lksg_img_large=$data['lksg_img_large'];
							$lksg_img_medium_auto=$data['lksg_img_medium_auto'];
							$lksg_img_large_auto=$data['lksg_img_large_auto'];
							$img_desc=$data['img_desc'];
							$lksg_video_link=$data['lksg_video_link'];
							$lksg_external_link=$data['lksg_external_link'];
							$lksg_portfolio_type=$data['lksg_portfolio_type'];
							?>
							<li class="choose_image_entry">
								<a href="" class="lksgallery_remove_image"><img src="<?php echo WGP_PLUGIN_URL.'images/Close-icon.png'; ?>"></a>
								<img src="<?php echo $lksg_img_thumb; ?>">
								<input type="text" name="lksg_image_url[]" id="lksg_image_url[]" value="<?php echo $url_c; ?>" readonly="readonly" style="display:none;" />
								<input type="text" id="lksg_img_thumb[]" name="lksg_img_thumb[]" class="lksg_label_text"  value="<?php echo $lksg_img_thumb; ?>"  readonly="readonly" style="display:none;" />
								<input type="text" id="lksg_img_medium[]" name="lksg_img_medium[]" class="lksg_label_text"  value="<?php echo $lksg_img_medium; ?>"  readonly="readonly" style="display:none;" />
								<input type="text" id="lksg_img_large[]" name="lksg_img_large[]" class="lksg_label_text"  value="<?php echo $lksg_img_large; ?>"  readonly="readonly" style="display:none;" />
								<input type="text" id="lksg_img_medium_auto[]" name="lksg_img_medium_auto[]" class="lksg_label_text"  value="<?php echo $lksg_img_medium_auto; ?>"  readonly="readonly" style="display:none;" />
								<input type="text" id="lksg_img_large_auto[]" name="lksg_img_large_auto[]" class="lksg_label_text"  value="<?php echo $lksg_img_large_auto; ?>"  readonly="readonly" style="display:none;" />


								<select name="lksg_portfolio_type[]" id="lksg_portfolio_type[]" style="width:100%;">
									<optgroup label="Select Type">
										<option value="image" <?php selected($lksg_portfolio_type, 'image' ); ?>><i class="fa fa-image"></i>Image </option>
										<option value="video" <?php selected($lksg_portfolio_type, 'video' ); ?>><i class="fa fa-youtube-play">Video </option>
										<option value="link" <?php selected($lksg_portfolio_type, 'link' ); ?>><i class="fa fa-link"></i>Link</option>
									</optgroup>
								</select>
								<label>Label</label>
								<input type="text" placeholder="Enter Image label" name="lksg_image_label[]" class="lksg_label_text" value="<?php echo $name; ?>">

								<label>Video URL <a href="http://weblizar.com/get-youtube-vimeo-video-url/" target="_blank"><strong>Help</strong></a> </label>
								<input type="text" id="lksg_video_link[]" name="lksg_video_link[]" placeholder="Enter Youtube/Vimeo Video URL" class="pgpp_label_text" value="<?php echo $lksg_video_link; ?>">

								<label>Link</label>
								<input type="text" id="lksg_external_link[]" name="lksg_external_link[]" placeholder="Enter Link URL" class="lksg_label_text" value="<?php echo $lksg_external_link; ?>">

								<label>Description</label>
								<textarea name="img_desc[]" id="img_desc[]" placeholder="Description"><?php echo $img_desc; ?></textarea>
							</li>
							<?php
						}
					}else{
						$TotalImg=0;
					}
					?>
				</ul>
				<div id="clearfix"></div>
			</div> 
			
			<div class="add_new_img"  data-uploader_title="Upload Image" data-uploader_button_text="Select">
				<div class="dashicons dashicons-plus"></div>
				<p>
					Add New Image
				</p>
			</div>
			<?php
		}

		public function my_cpt() {
			$labels = array(
				'name'               => _x( 'Gallery pro Weblizar', 'post type general name', 'test_gallery_doman' ),
				'singular_name'      => _x( 'Gallery pro Weblizar', 'post type singular name', 'test_gallery_doman' ),
				'menu_name'          => _x( 'Gallery pro Weblizar', 'admin menu', 'test_gallery_doman' ),
				'name_admin_bar'     => _x( 'Gallery pro Weblizar', 'add new on admin bar', 'test_gallery_doman' ),
				'add_new'            => _x( 'Add New Gallery', 'Gallery', 'test_gallery_doman' ),
				'add_new_item'       => __( 'Add New Gallery', 'test_gallery_doman' ),
				'new_item'           => __( 'New Gallery', 'test_gallery_doman' ),
				'edit_item'          => __( 'Edit Gallery', 'test_gallery_doman' ),
				'view_item'          => __( 'View Gallery', 'test_gallery_doman' ),
				'all_items'          => __( 'All Galleries', 'test_gallery_doman' ),
				'search_items'       => __( 'Search Galleries', 'test_gallery_doman' ),
				'parent_item_colon'  => __( 'Parent Galleries:', 'test_gallery_doman' ),
				'not_found'          => __( 'No Galleries found.', 'test_gallery_doman' ),
				'not_found_in_trash' => __( 'No Galleries found in Trash.', 'test_gallery_doman' )
				);

$args = array(
	'labels'             => $labels,
	'description'        => __( 'Description.', 'test_gallery_doman' ),
	'public'             => false,
	'publicly_queryable' => true,
	'show_ui'            => true,
	'show_in_menu'       => true,
	'query_var'          => true,
	'menu_icon'          => 'dashicons-format-gallery',
	'rewrite'            => array( 'slug' => 'gallery' ),
	'capability_type'    => 'post',
	'has_archive'        => true,
	'hierarchical'       => false,
	'menu_position'      => 67,
	//'taxonomies'         =>  array('category'),
	'supports'           => array( 'title' )
	);

register_post_type( 'acme_product', $args );

add_filter( 'manage_edit-acme_product_columns', array(&$this, 'acme_product_columns' )) ;
add_action( 'manage_acme_product_posts_custom_column', array(&$this, 'acme_product_manage_columns' ), 10, 2 );
}

function acme_product_columns( $columns ){
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Gallery' ),
		'shortcode' => __( 'Gallery Shortcode' ),
		'date' => __( 'Date' )
		);
	return $columns;
}

function acme_product_manage_columns( $column, $post_id ){
	global $post;
	switch( $column ) {
		case 'shortcode' :
		echo '<input type="text" value="[WG id='.$post_id.']" readonly="readonly" />';
		break;
		default :
		break;
	}
}

public function lks_admin_thumb($id)
{
	$img1=wp_get_attachment_image_src($id,'lksg_image_admin_original',true);
	$img_thumb=wp_get_attachment_image_src($id,'lksg_gallery_admin_thumb',true);
	$img_medium=wp_get_attachment_image_src($id,'lksg_gallery_admin_medium',true);
	$img_large=wp_get_attachment_image_src($id,'lksg_gallery_admin_large',true);
	$img_medium_auto=wp_get_attachment_image_src($id,'lksg_gallery_admin_medium_auto',true);
	$img_large_auto=wp_get_attachment_image_src($id,'lksg_gallery_admin_large_auto',true);
	
	
	$u_string=substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 5);
	?>	
	<li class="choose_image_entry">
		<a href="" class="lksgallery_remove_image">
			<img src="<?php echo WGP_PLUGIN_URL.'images/Close-icon.png'; ?>"></a>
			<img src="<?php echo $img_thumb[0]; ?>">
			<input type="text" id="lksg_image_url[]" name="lksg_image_url[]"  value="<?php echo $img1[0]; ?>" readonly="readonly" style="display:none;" />
			<input type="text" id="lksg_img_thumb[]" name="lksg_img_thumb[]" class="lksg_label_text"  value="<?php echo $img_thumb[0]; ?>"  readonly="readonly" style="display:none;" />
			<input type="text" id="lksg_img_medium[]" name="lksg_img_medium[]" class="lksg_label_text"  value="<?php echo $img_medium[0]; ?>"  readonly="readonly" style="display:none;" />
			<input type="text" id="lksg_img_large[]" name="lksg_img_large[]" class="lksg_label_text"  value="<?php echo $img_large[0]; ?>"  readonly="readonly" style="display:none;" />
			<input type="text" id="lksg_img_medium_auto[]" name="lksg_img_medium_auto[]" class="lksg_label_text"  value="<?php echo $img_medium_auto[0]; ?>"  readonly="readonly" style="display:none;" />
			<input type="text" id="lksg_img_large_auto[]" name="lksg_img_large_auto[]" class="lksg_label_text"  value="<?php echo $img_large_auto[0]; ?>"  readonly="readonly" style="display:none;" />


			<select name="lksg_portfolio_type[]" id="lksg_portfolio_type[]" style="width:100%;">
				<optgroup label="Select Type">
					<option value="image" selected="selected"><i class="fa fa-image"></i>Image </option>
					<option value="video"><i class="fa fa-youtube-play"></i>Video </option>
					<option value="link"><i class="fa fa-link"></i>Link</option>
				</optgroup>
			</select>

			<label>Label</label>
			<input type="text" placeholder="Enter Image label" name="lksg_image_label[]" class="lksg_label_text">

			<label>Video URL <a href="http://weblizar.com/get-youtube-vimeo-video-url/" target="_blank"><strong>Help</strong></a> </label>
			<input type="text" id="lksg_video_link[]" name="lksg_video_link[]" placeholder="Enter Youtube/Vimeo Video URL" class="pgpp_label_text">

			<label>Link</label>
			<input type="text" id="lksg_external_link[]" name="lksg_external_link[]" placeholder="Enter Link URL" class="lksg_label_text">

			<label>Description</label>
			<textarea name="img_desc[]" id="img_desc[]" placeholder="Description"></textarea>

		</li>
		<?php
	}

	public function lks_ajax_get_thumb()
	{
		echo $this->lks_admin_thumb($_POST['imageid']);
		die();
	}

	public function lksg_add_image_meta_box_save($postid)
	{			
		if(isset($postid) && isset($_POST['lksg_wl_action']))
		{
			$total_img=count($_POST['lksg_image_url']);
			$img_array=array();

			if($total_img)
			{
				for($i=0; $i < $total_img; $i++)
				{
					$img_label 				=  stripslashes($_POST['lksg_image_label'][$i]);
					$url 					=$_POST['lksg_image_url'][$i];
					$lksg_img_thumb			=$_POST['lksg_img_thumb'][$i];
					$lksg_img_medium		=$_POST['lksg_img_medium'][$i];
					$lksg_img_large			=$_POST['lksg_img_large'][$i];
					$lksg_img_medium_auto	=$_POST['lksg_img_medium_auto'][$i];
					$lksg_img_large_auto	=$_POST['lksg_img_large_auto'][$i];
					$lksg_video_link 		=$_POST['lksg_video_link'][$i];
					$lksg_external_link 	=$_POST['lksg_external_link'][$i];
					$img_desc 				=$_POST['img_desc'][$i];
					$lksg_portfolio_type 	=$_POST['lksg_portfolio_type'][$i];
					
					$img_array[]=array(
						'lksg_image_label' 		=>$img_label,
						'lksg_image_url' 		=> $url,
						'lksg_img_thumb' 		=> $lksg_img_thumb,
						'lksg_img_medium' 		=> $lksg_img_medium,
						'lksg_img_large' 		=> $lksg_img_large,
						'lksg_img_medium_auto' 	=> $lksg_img_medium_auto,
						'lksg_img_large_auto'	=> $lksg_img_large_auto,
						'lksg_video_link'		=>$lksg_video_link,
						'lksg_external_link'	=>$lksg_external_link,
						'lksg_portfolio_type'	=>$lksg_portfolio_type,
						'img_desc'				=>$img_desc
						);

					update_post_meta($postid, 'lksg_all_photos_details', base64_encode(serialize($img_array)));
					update_post_meta($postid, 'lksg_total_images_count', $total_img);
				}
			}else{

				$total_img=0;
				$img_array[]=array();
				update_post_meta($postid, 'lksg_all_photos_details', base64_encode(serialize($img_array)));
				update_post_meta($postid, 'lksg_total_images_count', $total_img);
			}
		}
	}

	public function lksg_settings_meta_save($PostID) {
		if(isset($PostID) && isset($_POST['lksg_action']))
		{
			$lk_gallery_title 		=$_POST['lk-show-gallery-title'];
			$lk_show_image_label 	=$_POST['lk-show-image-label'];
			$lk_Gallery_Layout  	= $_POST['lk-gallery-layout'];
			$wgp_Color				=$_POST['wgp_Color'];
			$lk_label_Color			=$_POST['lk_label_Color'];
			$lk_desc_font_Color		=$_POST['lk_desc_font_Color'];
			$lk_btn_Color			=$_POST['lk_btn_Color'];
			$lk_btn_font_Color		=$_POST['lk_btn_font_Color'];
			$lk_font_style			=$_POST['lk_font_style'];
			$lk_Custom_CSS			=$_POST['lk_Custom_CSS'];
			$lk_show_img_desc		=$_POST['lk_show_img_desc'];
			$lk_Light_Box		    =$_POST['lk_Light_Box'];
			$lk_open_link 			=$_POST['lk_open_link'];
			$lk_button_title		=$_POST['lk_button_title'];

			$lk_setting_array=serialize(array(

				'lk_gallery_title'=>$lk_gallery_title,
				'lk_show_image_label'=>$lk_show_image_label,
				'lk_Gallery_Layout'=>$lk_Gallery_Layout,
				'wgp_Color'=>$wgp_Color,
				'lk_label_Color'=>$lk_label_Color,
				'lk_desc_font_Color'=>$lk_desc_font_Color,
				'lk_btn_Color'=>$lk_btn_Color,
				'lk_btn_font_Color'=>$lk_btn_font_Color,
				'lk_font_style'=>$lk_font_style,
				'lk_Custom_CSS'=>$lk_Custom_CSS,
				'lk_show_img_desc'=>$lk_show_img_desc,
				'lk_Light_Box'=>$lk_Light_Box,
				'lk_open_link'=>$lk_open_link,
				'lk_button_title'=>$lk_button_title	

				));
			$lksg_Gallery_Settings = "lksg_Gallery_Settings_".$PostID;
			update_post_meta($PostID,$lksg_Gallery_Settings,$lk_setting_array);
		}
	}

}
global $lksg;
$lksg = lksg::forge();

require_once("gallery-shortcode.php");


add_action('media_buttons_context', 'lksg_custom_button',17);
add_action('admin_footer', 'lksg_inline_popup_content');
function lksg_custom_button($context) {  

	$btnimg = WGP_PLUGIN_URL.'/images/Photos-icon.png';  
	$lksgcontainer_id = 'lksg_div';
  //append the icon
	$context .= '<a class="button button-primary thickbox"  title="'."Select Gallery to insert into post".'"  
	href="#TB_inline?width=400&inlineId='.$lksgcontainer_id.'">
	<span class="wp-media-buttons-icon" style="background: url('.$btnimg.'); background-repeat: no-repeat; background-position: left bottom;"></span>
	Gallery Pro Shortcode</a>';

	return $context;
}

function lksg_inline_popup_content() {
	?>
	<script type="text/javascript">

	jQuery(document).ready(function() {

		jQuery('#lksg_galleryinsert').on('click', function() {				
			var id = jQuery('#lksg-gallery-select option:selected').val();				
			window.send_to_editor('<p>[WG id=' + id + ']</p>');
			tb_remove();
		})
	});
	</script>
	?>
	<div id="lksg_div" style="display:none;">
		<h2>Select Gallery To Insert Into Post</h2>
		<?php 
		$all_posts = wp_count_posts( 'acme_product')->publish;
		$args = array('post_type' => 'acme_product', 'posts_per_page' =>$all_posts);
		global $lksg_galleries;
		$lksg_galleries = new WP_Query( $args );			
		if( $lksg_galleries->have_posts() ) { ?>	
		<select id="lksg-gallery-select">
			<?php
			while ( $lksg_galleries->have_posts() ) : $lksg_galleries->the_post(); ?>
			<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
			<?php
			endwhile; 
			?>
		</select>
		<button class='button primary' id='lksg_galleryinsert'>Insert Gallery Shortcode</button>
		<?php
	} else { 
		echo "No Gallery found";
	}
	?>
</div>
<?php
}
?>